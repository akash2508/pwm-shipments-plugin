<?php

namespace ag2508\stamps\api;

/**
 * Exception thrown for whatever reason.
 */
class Exception extends \Exception
{
}

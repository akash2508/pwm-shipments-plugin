<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteConnector
 *
 * @author CallMeTim
 */
abstract class PWMSiteConnector {
    protected $host;
    protected $user;
    protected $pass;
    protected $baseDir;
    protected $port;

    public $connected = FALSE;
    public $connector;
    
    /**
     * 
     * @param type $host
     * @param type $user
     * @param type $pass
     * @param type $baseDir 
     */
    public function __construct($host, $user, $pass, $baseDir, $port) {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->baseDir = pwmWPForceTrailingSlash($baseDir);
        $this->port = $port;
    }
    
    abstract public function connect();
    
    abstract public function login();
    
    abstract public function saveFile($source, $dest);
    
    abstract public function getFile($pathToSaveContents, $filePath, $mode);

    abstract public function __destruct();
}

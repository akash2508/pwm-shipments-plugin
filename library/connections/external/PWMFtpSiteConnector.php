<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// https://code.google.com/p/ftp-php/
require_once PWM_SHIPMENTS_PLUGIN_ROOT_DIR . 'library/connections/external/abstract/PWMSiteConnector.php';

class PWMFtpSiteConnector extends PWMSiteConnector {
    
    public function __construct($host, $user, $pass, $baseDir, $port = 21) {
        parent::__construct($host, $user, $pass, $baseDir, $port);
        
        require_once PWM_SHIPMENTS_PLUGIN_ROOT_DIR . 'library/connections/external/PWMFtp.php';
        $this->connector = new PWMFtp;
        
        $this->connect();
        $this->login();
    }
    
    public function connect() {
        $this->connector->connect($this->host);
    }
    
    public function login() {
        $this->connector->login($this->user, $this->pass);
    }
    
    public function saveFile($source, $dest) {
        $destination = $this->baseDir . $dest;
        $this->connector->put($destination, $source, FTP_BINARY);
    }
    
    public function saveUrl($url, $dest) {
        $data = file_get_contents($url);
        $source = tmpfile();
        file_put_contents($source, $data);
        $this->saveFile($source, $dest);
        unlink($source);
    }
    
    public function __destruct() {
        $this->connector->close();
    }
    
    public function scandir($dir = ".") {
        return $this->connector->nlist($dir);
    }
    
    public function rawlist($dir = ".") {
        return $this->connector->rawlist($dir);
    }

    /**
     * Writes remote file to local file
     * @param String $localFile
     * @param String $remoteFile
     * @param int $mode
     * @return \FtpException
     */
    public function getFile($localFile, $remoteFile, $mode = FTP_ASCII) {
        $response = true;
        try {
            $this->connector->get($localFile, $remoteFile, $mode);
        } catch( FtpException $ex ) {
            $response = $ex;
        }
        return $response;
    }
    
    public function readFile($remoteFile, $mode = FTP_ASCII){
        $response = false;
        $tmp = tmpfile();
        $metaDatas = stream_get_meta_data($tmp);
        $tmpFilename = $metaDatas['uri'];
        
        $getFile = $this->getFile($tmpFilename, $remoteFile, $mode);
        if( $getFile === true ) {
            $response = file_get_contents($tmpFilename);
        }
        fclose($tmp);
        return $response;
    }
    
    public function removeFile($file) {
        return $this->connector->delete($file);
    }
    
    /**
     * 
     * @param type $from
     * @param type $to
     * @return bool 
     */
    public function moveFile($from, $to) {
        return $this->connector->rename($from, $to);
    }
    
    /**
     * 
     * @param String or Resource $localFile
     * @param String $remoteFile
     * @param int $mode
     * @return \FtpException
     */
    public function putFile( $localFile, $remoteFile, $mode = FTP_ASCII) {
        $response = true;
        try {
            $remoteDest = $this->baseDir . $remoteFile;
            if( is_string($localFile) ) {
                $local = fopen($localFile, "r");
            } else {
                $local = $localFile;
            }
            
            $this->connector->fput($remoteDest, $local, $mode);
            
            if( is_string($localFile) ) {
                fclose($local);
            }
        } catch( FtpException $ex ) {
            $response = $ex;
        }
        return $response;
    }
    
    /**
     * Write content to remote server
     * if isAppend then get current file and append then save
     * @param String $remoteFile
     * @param String $content
     * @param bool $isAppend
     * @param int $ftpMode
     * @return \FtpException
     */
    public function write($remoteFile, $content, $isAppend = true, $ftpMode = FTP_ASCII) {
        $response = true;
        try {
            $remoteDest = $this->baseDir . $remoteFile;
            
            $localFile = tempnam(PWM_SHIPMENTS_PLUGIN_ROOT_DIR.'tmp/', null);
            if( $isAppend ) {
                //load current content into local file first
                $this->getFile($localFile, $remoteFile);
            }
            $local = fopen($localFile, "a+");
            fwrite($local, $content);
            fclose($local); //save content
            $local = fopen($localFile, "r"); //reload handle after saving content
            
            $this->connector->fput($remoteDest, $local, $ftpMode);
            
            //deconstruct files
            fclose($local);
            unlink($localFile);
        } catch( FtpException $ex ) {
            $response = $ex;
        }
        
        // ob_start();
        // var_dump($this->baseDir, $remoteFile, $remoteDest, $response);
       // pwmDebug("FTP Write Response \n" . ob_get_clean());
        
        return $response;
    }
    
    public function fileExists($path) {
        return $this->connector->size($path) === -1 ? false : true;
    }
    
    /**
     * Magic method - call connector method if exists else throw error
     * @param type $name
     * @param type $arguments
     * @throws Exception
     */
    public function __call($name, $arguments) {
        if( is_callable(array($this->connector, $name)) ) {
            return call_user_func_array(array($this->connector, $name), $arguments);
        } else {
            throw new Exception("Function '$name' not found");
        }
    }

}

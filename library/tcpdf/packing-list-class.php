<?php

function pwm_shipping_tcpdf_packing_list_class() {
    if (class_exists('PWM_Shipping_TCPDF_Packing_List'))
        return;

    require_once PWM_PLUGIN_DIR . 'assets/tcpdf/tcpdf.php';

    class PWM_Shipping_TCPDF_Packing_List extends TCPDF {
        
        //Page header
        public function Header() {
            $this->SetY(5);
            // Logo
            $header = "\n" . date('m/d/Y');
            $this->Cell(0, 0, $header, 0, 0, 'C');
        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            $footer = sprintf(
                    "Page %s of %s",
                    $this->getAliasNumPage(), $this->getAliasNbPages());
            $this->Cell(0, 0, $footer, 0, 0, 'C');
        }

    }

}

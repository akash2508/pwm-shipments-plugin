<?php
// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {

    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

}
/**
 * Create a new shipment table class that will extend the WP_List_Table
 */
if( !class_exists("PWM_Shipments_Akademos_POfiles") ) {
    
    class PWM_Shipments_Akademos_POfiles extends WP_List_Table
    {   
        public $data;
        public $statuses = array(
            1 => "Processed",
            2 => "Shipping Error",
        );

        /**
         * Run a constructor to fetch data from the custom database table "wp_pwm_akademos_po".
         *
         * @return Void
        */
        function __construct() {
            parent::__construct();
            global $wpdb;
            $query = "SELECT * FROM {$wpdb->prefix}pwm_akademos_po ORDER BY `id` DESC";
            $rows = $wpdb->get_results($query, ARRAY_A);
            
            foreach($rows as $index => $row) {
                $row['date'] = date("d-m-Y", strtotime($row['date']));
                $rows[$index] = $row;
            }
            
            $this->data = $rows;
        }

        /**
         * check number of nows exist for PO files/data.
         *
         * @return Void
         */
        function hasRows() {
            return ( count($this->data) > 0 );
        }

        /**
         * Set columns to display in WP Table
         *
         * @return Void
         */

        function get_columns() {
            $columns = array(
                'status' => 'Status',
                'path' => 'File Path',
                'name' => 'File Name',
                'date' => 'Processed Date',
            );
            return $columns;
        }

        /**
         * Prepare the items for the table to process
         *
         * @return table data
         */
        function prepare_items() {
            global $wpdb;
            
            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();

            $perPage = get_option('posts_per_page');
            $currentPage = $this->get_pagenum();
            $totalItems = count($this->data);

            $this->set_pagination_args(array(
                'total_items' => $totalItems,
                'per_page' => $perPage
            ));

            usort( $this->data, array( &$this, 'sort_data' ) ); 
            
            if( is_array($this->data) ) {
                $data = array_slice($this->data, (($currentPage - 1) * $perPage), $perPage);
            } else {
                $data = array();
            }
            
            //parse data load specific fields to data
            foreach( $data as $index => $row ) {
                $row['status'] = $this->getStatusStr( intval($row['status']) );
                //update data w/new rows
                $data[$index] = $row;
            }

            $this->_column_headers = array($columns, $hidden, $sortable);
            $this->items = $data;
        }
        
        /**
         * get status of shipments
         *
         * @return Status
         */
        public function getStatusStr($status) {
            return isset($this->statuses[$status]) ? $this->statuses[$status] : 'N/A';
        }

        /**
         * Add actions/links to the columns
         *
         * @return Actions
         */

        function column_status($item) {
            $page = empty($_REQUEST['page']) ? '' : $_REQUEST['page'];
            $id = intval($item['id']);
            
            $actions = array(
                'download' => sprintf('<a href="?page=%s&action=%s&id=%s">Download</a>', $page, 'download', $id),
                'delete' => sprintf('<a class="confirm-click" href="?page=%s&action=%s&id=%s">Delete</a>', $page, 'delete', $id),
                'create_new' => sprintf('<a href="?page=%s&action=%s&id=%s">Create new shipping file</a>', $page, 'create_new', $id),
            );
            
            return sprintf('%1$s %2$s', $item['status'], $this->row_actions($actions));
        }

        /**
         * Render a column when no column specific method exists.
         *
         * @param array $item
         * @param string $column_name
         *
         * @return mixed
         */
        function column_default($item, $column) {
            $output = "N/A";
            if( ! empty($item[$column]) ) {
                $output = $item[$column];
            }
            echo $output;
        }

        /**
         * Render the bulk edit checkbox
         *
         * @param array $item
         *
         * @return string
         */
        function column_cb($item) {
            return sprintf(
                '<input type="checkbox" name="id[]" value="%s" />', $item['id']
            );
        }

        /**

         * Define the sortable columns

         *

         * @return Array

        */

        public function get_sortable_columns()
        {

            return array('date' => array('date', true));

        }

        /**
         * Allows you to sort the data by the variables set in the $_GET
         *
         * @return Mixed
         */
        private function sort_data( $a, $b )

        {
            // Set defaults
            $orderby = 'date';

            $order = 'desc';
            // If orderby is set, use this as the sort column
            if(!empty($_GET['orderby']))
            {

                $orderby = $_GET['orderby'];

            }

            // If order is set use this as the order

            if(!empty($_GET['order']))
            {
                $order = $_GET['order'];
            
            }            

            
            if($order === 'asc')
            {

                if ($a[$orderby] == $b[$orderby]) return 0;
                
                return (strtotime($a[$orderby]) > strtotime($b[$orderby])) ? 1 : -1;
            
            }else{

                if ($a[$orderby] == $b[$orderby]) return 0;
                
                return (strtotime($a[$orderby]) < strtotime($b[$orderby])) ? 1 : -1;
            }

        }
        
    }
} 
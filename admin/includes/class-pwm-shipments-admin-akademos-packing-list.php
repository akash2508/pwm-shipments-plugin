<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the akademos packing list and search by date functionality 
 * It will display revenu and report of books, orders and customers.
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin/includes
 * @author     PWM
 */
if( !class_exists("pwm_shipments_admin_akademos_packing_list") ) {
    
    class pwm_shipments_admin_akademos_packing_list {   
        /**
         * Run a constructor.
         *
         * @return Void
        */

        public function __construct() {
            
        }

        /**
         * Packing list search form()
         *
         * @return data
        */
        public function pwm_shipments_packing_list_form(){
            
            global $wpdb;
    
            $date = ( isset($_REQUEST['pwm-date']) && pwm_shipments_validateDateString($_REQUEST['pwm-date']) ) ? $_REQUEST['pwm-date'] : date("Y-m-d") ;
            
            $dateRedirectBase = admin_url("admin.php?page=pwm-shipping-akademos-packing-list&pwm-date=");
            
            $statusOptions = array(0 => 'Any', 1 => 'Inactive', 2 => 'Active');
            
            $status = isset($_REQUEST['pwm-status']) ? $_REQUEST['pwm-status'] : 0;

            $query = "SELECT *, `{$wpdb->prefix}pwm_shipping_all_orders`.`woo_order_id` FROM `{$wpdb->prefix}pwm_shipping_akademos_orders`"
                    . "\n LEFT JOIN `{$wpdb->prefix}pwm_shipping_all_orders` "
                    . "\n ON `{$wpdb->prefix}pwm_shipping_akademos_orders`.`id` = `{$wpdb->prefix}pwm_shipping_all_orders`.`type_id` "
                    . "\n WHERE "
                    . "\n     `{$wpdb->prefix}pwm_shipping_all_orders`.`type` = 'akademos' "
                    . "\n     AND `{$wpdb->prefix}pwm_shipping_akademos_orders`.`label_base64` != ''"
                    . "\n     AND  `{$wpdb->prefix}pwm_shipping_all_orders`.`created` > '$date 00:00:00' "
                    . "\n     AND  `{$wpdb->prefix}pwm_shipping_all_orders`.`created` < '$date 23:59:59' "
                    . "\n ORDER BY `shipper` ASC,  `shipment_method` ASC  ";
            
            $orders = $wpdb->get_results($query);
            
            ?>
            <div class="packinglist-form-div">
                <form action="" method="GET" class="pwm-form">
                    <h3>Filter</h3>
                    
                    <div class="col-xs-6">
                        <label class="form-label">Date</label>
                    </div>
                    <div class="col-xs-6">
                        <input class="form-control" type="date" max="<?php echo date("Y-m-d"); ?>" name="pwm-date" value="<?=$date?>" />
                    </div>
                    
                    <div class="col-xs-6">
                        <label class="form-label">Status</label>
                    </div>
                    <div class="col-xs-6">
                        <?=pwm_shipments_general_frontend_select('name="pwm-status" class="form-control"', $statusOptions, $status) ?>
                    </div>
                    <div class="col-xs-offset-9 col-xs-3">
                        <input type="hidden" name="page" value="<?=$_REQUEST['page']?>" />
                        <input type="submit" class="btn btn-default form-control" value="Search" />
                    </div>
                </form>
            </div>    
            <?php
            
            if (count($orders) > 0) {

                $tableData = array(
                    'open' => "<div class='packinglist-div'><table cellspacing=\"0\" cellpadding=\"1\" border=\"1\" class='wp-list-table widefat fixed striped'>",
                    'headings' => "<thead><tr>"
                        . "     <th colspan=\"2\">Barcode</th>"
                        . "     <th>Qty</th>"
                        . "     <th colspan=\"3\">Name</th>"
                        . "     <th colspan=\"3\">Address 1</th>"
                        . "     <th colspan=\"2\">Address 2</th>"
                        . "     <th colspan=\"2\">City</th>"
                        . "     <th>State</th>"
                        . "     <th colspan=\"3\">Postcode</th>"
                        . "     <th colspan=\"2\">Shipment Method</th>"
                        . "     <th colspan=\"4\">Tracking #</th>"
                        . "</tr></thead>"
                        . "<tbody>",
                    'close' => "</tbody></table></div>",
                    'rows' => array(),
                );
                $totals = array();
                $orderCount = 0;
                $packingList = $tableData['open'] . $tableData['headings'];
                foreach( $orders as $row => $order ) {
                    //generate formatted line
                    $bookCode = null;
                    $bookQty = 0;

                    $style = pwm_shipments_general_pdf_get_row_style($orderCount);
                    $line = "<tr style=\"$style\">";
                    foreach( explode("|", $order->input_line) as $i => $inputPart ) {
                        //told not to include first 2 #s of PO
                        if( $i < 2 ) continue;
                        
                        if( $i === 2 ) {
                            $bookCode = intval($inputPart);
                            if( isset($_REQUEST['test']) ) {
                                $inputPart = "$row => $inputPart";
                            }
                        }
                        if( $i === 3 ) $bookQty = intval($inputPart);

                        // address1, tracking#
                        if( in_array($i, array(2, 6, 7, 10)) ) {
                            $line .= "<td colspan=\"2\">$inputPart</td>";
                        } else if( in_array($i, array(4, 5, 9)) ) {
                            $line .= "<td colspan=\"3\">$inputPart</td>";
                        } else {
                            $line .= "<td>$inputPart</td>";
                        }
                    }
                    $line .= "<td colspan=\"4\">".$order->tracking."</td></tr>";

                    $tableData['rows'][] = $line;
                    $packingList .= $line;

                    //update totals
                    if ( isset($totals[$bookCode]) ) {
                        $totals[$bookCode] += $bookQty;
                    } else {
                        $totals[$bookCode] = $bookQty;
                    }

                    $orderCount++;
                }
                $packingList .= $tableData['close'];
                //sort and get totals string
                $totalTable = $tableData['open']
                        . "<thead><tr>"
                        . "     <th>Barcode</th>"
                        . "     <th># of Orders</th>"
                        . "     <th>Price Per Unit</th>"
                        . "     <th>Total Sales</th>"
                        . "</tr></thead>"
                        . "<tbody>";
                ksort($totals);

                $totalSales = 0;
                $totalOrders = 0;
                $totalsCount = 0;
                foreach($totals as $k => $v ) {
                    $style = pwm_shipments_general_pdf_get_row_style($totalsCount);
                    $code = $k;
                    $numOfOrders = $v;
                    $totalOrders += $numOfOrders;
                    $pricePerUnit = pwm_shipments_akademos_get_price_per_unit($code);
                    $pricePerUnitFormatted = number_format($pricePerUnit, 2);
                    $bookTotalSales = number_format($pricePerUnit * $numOfOrders, 2);
                    $totalSales += $bookTotalSales;
                    $totalTable .= "<tr style=\"$style\">"
                            . "     <td><center>$code</center></td>"
                            . "     <td><center>$numOfOrders</center></td>"
                            . "     <td><center>\$$pricePerUnitFormatted</center></td>"
                            . "     <td><center>\$$bookTotalSales</center></td>"
                            . "</tr>";
                    $totalsCount++;
                }
                $totalSalesFormatted = number_format($totalSales, 2);
                $totalTable .= "<tr class=\"book-orders\">"
                        . "<td style='text-align:right'>Total Orders</td>"
                        . "<td>$totalOrders</td>"
                        . "<td style='text-align:right'>Todays Sales</td>"
                        . "<td>\$$totalSalesFormatted</td>"
                        . "</tr>";
                $totalTable .= $tableData['close'];
                ?>
                <form method="post" class="download-form">
                    <input type="hidden" name='query' value="<?= base64_encode($query) ?>" />
                    <input type="hidden" name='po-table' value="<?= base64_encode(serialize($tableData)) ?>" />
                    <input type="hidden" name='totals' value="<?= base64_encode($totalTable) ?>" />
                    <input type="hidden" name='pwm-test' value="" />
                     <input type="hidden" name="action" value="pwm_shipments_download_packinglist_hook">
                    <input type='submit' class='btn btn-default download-btn' name='download-now' value ='Download All Labels' />
                </form>
                <?= $packingList ?>
                <hr />
                <?= $totalTable ?>
                <?php
            } else {
                echo "<p>Sorry no labels currently available</p>";
            }
        
        }

    }   

} 
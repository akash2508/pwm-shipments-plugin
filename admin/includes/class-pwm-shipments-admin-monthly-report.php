<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the Monthly report and search by date functionality 
 * It will display and provide pdf file for monthly/quaterly reports.
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin/includes
 * @author     PWM
 */
if( !class_exists("PWM_Shipments_Admin_Monthly_Report") ) {
    
    class PWM_Shipments_Admin_Monthly_Report {   
        /**
         * Run a constructor.
         *
         * @return Void
        */

        public function __construct() {
            
        }

        /**
         * Report Generator Form.
         *
         * @return Void
        */
        public function report_form(){
            global $wpdb;

            $now = current_time('mysql');

            $pwmShippingOrders = $wpdb->get_results("SELECT DISTINCT YEAR(created) AS `year`,
                MONTH(created) AS `month`
                FROM {$wpdb->prefix}pwm_shipping_all_orders WHERE created < '$now' AND status = 1
                GROUP BY YEAR(created), MONTH(created) ORDER BY created DESC");

            $form_html = '<form action="'.esc_attr( admin_url('admin-post.php')).'" method="post" class="pwm-form">
            <div class="col-xs-6">
            <label class="form-label">Time Period</label>
            <select name="pwm-date" class="small">';
            foreach($pwmShippingOrders as $orderEntry) {
                // Add leading zero
                $thisMonth = zeroise($orderEntry->month,2);

                // value to be passed into query string
                $form_html .= $this->get_quarter_option($thisMonth, $orderEntry->year);
                $form_html .= $this->get_year_option($thisMonth, $orderEntry->year);
                $form_html .= $this->get_month_option($thisMonth, $orderEntry->year);
                
            }
            $form_html .= '</select>';
            $form_html .= '<input type="hidden" name="action" value="pwm_shipments_monthly_report" />
                            <input type="submit" class="btn btn-default form-control" value="Get Report" />
                            </div>
                        </form>';

            return $form_html;
        
        }

        /**
         * Get month dropdown option
         *
         * @return Html
         */
        public function get_month_option($mo, $year) {
            global $month;
            $option = '';

            $thisM = strtotime($year.'-'.$mo.'-1');
            $option .= "<option value=\"$thisM\"";
            
            // Preselect currently-viewed month
            if ($thisM == isset($_POST['pwm-date'])) {
                $option .= ' selected="selected" ';
            }
            $option .= '>'.$month[$mo].', '.$year.'</option>';

            return $option;

        }

        /**
         * Get year quarter dropdown option
         *
         * @return Html
         */
        public function get_quarter_option($month, $year) {
            if(($month%3)==0) {
                if(ceil($month / 3)==1) {
                    $start_date = strtotime('1-January-'.$year);
                    $end_date = strtotime('1-March-'.$year);
                }
                if(ceil($month / 3)==2) {
                    $start_date = strtotime('1-April-'.$year);
                    $end_date = strtotime('1-June-'.$year);
                }
                if(ceil($month / 3)==3) {
                    $start_date = strtotime('1-July-'.$year);
                    $end_date = strtotime('1-September-'.$year);
                }
                if(ceil($month / 3)==4) {
                    $start_date = strtotime('1-October-'.$year);
                    $end_date = strtotime('1-December-'.$year);
                }
                return '<option value="'.$start_date.'-'.$end_date.'">Quarter '. ceil($month / 3).', '.$year.'</option>';
            }
        }

        /**
        * Get year dropdown option
        *
        * @return Html
        */
        public function get_year_option($month, $year) {
            if($month==12) {
                $start_date = strtotime('1-January-'.$year);
                $end_date = strtotime('31-December-'.$year);
                return '<option value="'.$start_date.'-'.$end_date.'">Yearly Report - '.$year.'</option>';
            }
        }

        /**
        * Get price per unit (per row)
        *
        * @return String
        */
        public static function get_price_per_unit($bookCode) {
            if( ! isset($GLOBALS['inventory-array']) ) {
                //only interact w/database once
                $GLOBALS['inventory-array'] = get_option('pwm-shipping-akademos-inventory-arr', array());
            }

            $price = 0.00;
            foreach( $GLOBALS['inventory-array'] as $row ) {
                if( $row[1] === $bookCode ) {
                    $price = $row[2];
                    break;
                }
            }
            return $price;
        }

        /**
        * Set pdf table row style
        *
        * @return CSS
        */
        public static function get_pdf_row_style($row) {
            $style = "font-size:large;padding:2px;";
            $style .= ( $row % 2 === 1 ) ? "background-color: rgb(204, 204, 204);" : "background-color: rgb(255, 255, 255);";
            return $style;
        }

        /**
        * Uppend data to pdf in table format
        *
        * @return Html
        */
        public static function wirte_pdf($time) {
            $month = self::get_month_order_report_month($time);
            $year = self::get_month_order_report_year($time);
            $content = self::create_html_order_content($time);

            $fileName = "$year-$month.pdf";

            require_once PWM_SHIPMENTS_PLUGIN_ROOT_DIR . 'library/tcpdf/tcpdf.php';
            
            $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Pro Web Marketing');
            $pdf->SetTitle($fileName);
            $pdf->SetSubject('Shipping Labels');
            
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            
            // set font
            $pdf->SetFont('times', '', 14);

            // add a page
            $pdf->AddPage();
            $pdf->writeHTML( nl2br($content), true, false, true, false, '');
            $pdf->Output($fileName, "D");
        }

        /**
        * Get selected month order rows
        *
        * @return Array
        */
        public function get_shipping_orders($time) {
            global $wpdb;
            $first_second = self::get_month_first_second($time);
            $last_second = self::get_month_last_second($time);

            $sql = $wpdb->prepare(" SELECT `inventory_id`, `qty` FROM `{$wpdb->prefix}pwm_shipping_all_orders` WHERE `created` > %s AND `created` < %s ", $first_second, $last_second);
            $sales = $wpdb->get_results( $sql, ARRAY_A );
            
            $reportRows = array();
            
            foreach( $sales as $sale ) {
                $inventoryId = $sale['inventory_id'];
                $qty = intval($sale['qty']);
                if( empty($reportRows[ $inventoryId ]) ) $reportRows[ $inventoryId ] = 0;
                
                $reportRows[ $inventoryId ] += $qty;
            }

            return $reportRows;
        }

        /**
        * Build order html content
        *
        * @return Html
        */
        public static function create_html_order_content($time) {
            $pwmmreport = new PWM_Shipments_Admin_Monthly_Report();
            $reportRows = $pwmmreport->get_shipping_orders($time);
            $month = self::get_month_order_report_month($time);
            $year = self::get_month_order_report_year($time);

            ksort($reportRows);

            $content = "<h4>$year $month Report</h4>"
                    . "<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\"><thead>"
                    . "<tr><td>Inventory ID</td><td>Quantity</td><td>Unit Price</td><td>Sales</td></tr>"
                    . "</thead><tbody>";
            $totalSales = 0.00;
            $rowCount = 0;

            foreach( $reportRows as $inventoryId => $qty ) {
                if($inventoryId) {
                    $unitPrice = self::get_price_per_unit($inventoryId);
                    $monthlySales = $unitPrice * $qty;
                    $totalSales += $monthlySales;
                    $content .= sprintf("<tr style=\"%s\"><td>%s</td><td>%s</td><td>$%s</td><td>$%s</td></tr>",
                            self::get_pdf_row_style($rowCount),
                            $inventoryId, $qty, number_format($unitPrice, 2), number_format($monthlySales, 2)
                    );
                    $rowCount++;
                }
            }
            $content .= sprintf("<tr><td></td><td></td><td><b></b>Total Sales</td><td>$%s</td></tr>", number_format($totalSales,2));
            $content .= "</tbody></table>";

            return $content;
        }

        /**
        * Get month first second
        *
        * @return String
        */
        public static function get_month_first_second($time) {
            if (strpos($time, '-') !== false) {
                $dates = explode('-',$time);
                $first_second = date('Y-m-d 00:00:00',$dates[0]);
            } else {
                $first_second = date('Y-m-d 00:00:00', $time);
            }
            return $first_second;
        }

        /**
        * Get month last second
        *
        * @return String
        */
        public static function get_month_last_second($time) {
            if (strpos($time, '-') !== false) {
                $dates = explode('-',$time);
                $last_second = date('Y-m-t 23:59:59',$dates[1]);
            } else {
                $last_second  = date('Y-m-t 23:59:59', $time);
            }
            return $last_second;
        }

        /**
        * Get order report month
        *
        * @return String
        */
        public static function get_month_order_report_month($time) {
            if (strpos($time, '-') !== false) {
                $dates = explode('-',$time);
                $month = date('F',$dates[0]).'-'.date('F',$dates[1]);
            } else {
                $month = date('F',$time);
            }
            return $month;
        }

        /**
        * Get order report year
        *
        * @return String
        */
        public static function get_month_order_report_year($time) {
            if (strpos($time, '-') !== false) {
                $dates = explode('-',$time);
                $startYear = date('Y',$dates[0]);
                $endYear = date('Y',$dates[1]);
                if($startYear == $endYear){
                    $year = $startYear;
                }else{
                    $year = $startYear.'-'.$endYear;
                }
            } else {
                $year = date('Y',$time);
            }
            return $year;
        }
    }
} 
<?php
// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {

    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
/**
 * Create a new shipment table class that will extend the WP_List_Table
 */
class PWM_Shipments_List_Table extends WP_List_Table
{
    /**
     * Run a constructor to fetch data from the custom database table "wp_pwm_shipping_all_orders".
     *
     * @return Void
     */
    public $data;

    public $statuses = array(

        1 => 'Label Printed',

        2 => 'Active',

        3 => 'Label Error',

    );

    function __construct() {

        parent::__construct();

        global $wpdb;

        $query = "SELECT * FROM {$wpdb->prefix}pwm_shipping_all_orders ORDER BY `id` DESC";//`status` DESC";

        $this->data = $wpdb->get_results($query, ARRAY_A);

    }

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */

    function prepare_items() {

            global $wpdb;

            $columns = $this->get_columns();

            $hidden = array();

            $sortable = $this->get_sortable_columns();

            $perPage = get_option('posts_per_page');

            $currentPage = $this->get_pagenum();

            $totalItems = count($this->data);

            $this->set_pagination_args(array(
                'total_items' => $totalItems,
                'per_page' => $perPage
            ));

            usort( $this->data, array( &$this, 'sort_data' ) );

            if( is_array($this->data) ) {

                $data = array_slice($this->data, (($currentPage - 1) * $perPage), $perPage);

            } else {

                $data = array();

            }

            //parse data load specific fields to data

            foreach( $data as $index => $row ) {

                $row['status'] = $this->getStatusStr($row['status']);

                $rowQuery = sprintf("SELECT * FROM {$wpdb->prefix}pwm_shipping_%s_orders WHERE `id` = %d ", 

                    $row['type'], intval($row['type_id'])

                );

                $typeRow = $wpdb->get_row($rowQuery, ARRAY_A);

                $row['shipper'] = $typeRow['shipper'];

                $row['shipment_method'] = $typeRow['shipment_method'];

                $row['tracking'] = $typeRow['tracking'];

                $data[$index] = $row;

            }

            $this->_column_headers = array($columns, $hidden, $sortable);

            $this->items = $data;

        }



    /**

     * Check number of rows in a table

     *

     * @return Array

     */    

    function hasRows() {

        return ( count($this->data) > 0 );

    }  

    /**

     * Override the parent columns method. Defines the columns to use in your listing table

     *

     * @return Array

     */

    public function get_columns()

    {

        $columns = array(

            'id' => 'ID',

            'type' => 'Type',

            'status' => 'Status',

            'shipper' => 'Shipper',

            'shipment_method' => 'Shipment Method',

            'tracking' => 'Tracking No.',

        );

        return $columns;

    }



    /**

     * Define which columns are hidden

     *

     * @return Array

     */

    public function get_hidden_columns()

    {

        return array();

    }



    /**

     * Define the sortable columns

     *

     * @return Array

     */

    public function get_sortable_columns()
    {

        return array('id' => array('id', true));

    }



    /**

     * Define what data to show on each column of the table

     *

     * @param  Array $item        Data

     * @param  String $column_name - Current column name

     *

     * @return Mixed

     */

    function column_default($item, $column) {

        $output = "N/A";

        if( ! empty($item[$column]) ) {

            $output = $item[$column];

        }

        echo $output;

    }



    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )

    {
        // Set defaults

        $orderby = 'id';

        $order = 'asc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {

            $orderby = $_GET['orderby'];

        }

        // If order is set use this as the order

        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strnatcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {

            return -$result;
        }

        return $result;
    }

    /**
     * Add actions/links to the columns
     *
     * @return Actions
     */

    function column_id($item) {

        $page = empty($_REQUEST['page']) ? '' : $_REQUEST['page'];

        $id = intval($item['id']);

        $msg = 'Are you sure?';

        $actions = array(

            'label' => sprintf('<a href="?page=%s&action=%s&id=%s">Download Label</a>', $page, 'download-label', $id),

            'delete' => sprintf('<a class="confirm-click"  href="?page=%s&action=%s&id=%s">Delete</a>', $page, 'delete', $id),

        );

        $hasLabel = false;

        if( $hasLabel ) {

            $actions['print-label'] = sprintf('<a target="_blank" href="?page=%s&action=%s&id=%s">Print Label</a>', $page, 'print-label', $id);

        }

        return sprintf('%1$s %2$s', $item['id'], $this->row_actions($actions));

    }

    /**
        * Render the bulk edit checkbox
        *
        * @param array $item
        *
        * @return string
    */
    function column_cb($item) {

        return sprintf(

            '<input type="checkbox" name="id[]" value="%s" />', $item['id']

        );

    }

    public function getStatusStr($status) {

        return isset($this->statuses[$status]) ? $this->statuses[$status] : 'N/A';

    }


}
<?php

if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class for handling po errors manually that will extend the WP_List_Table
 */
class PWM_Shipments_Order_Errors_Table extends WP_List_Table
{
	protected $connection, $errorDir;

	protected $statuses = array(
		1 => "Processed",
		2 => "Shipping Error",
	);
    
    public $filesToProcess, $processedFilesCount, $processedOrders;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct( array(
			'singular' => __( 'Manually Handle Error', 'pwm-shipments' ), //singular name of the listed records
			'plural'   => __( 'Manually Handle Errors', 'pwm-shipments' ), //plural name of the listed records
			'ajax'     => true //does this table support ajax?
		) );

		$this->connection = new PWMFtpSiteConnector(PWM_SHIPMENTS_AKADEMOS_FTP_HOST, PWM_SHIPMENTS_AKADEMOS_FTP_USER, PWM_SHIPMENTS_AKADEMOS_FTP_PASS, PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR);

		$this->errorDir = 'shipping/error/';

		$this->filesToProcess = array();
	}

	/**
	 * Retrieve error files from akademos ftp shipping error folder
	 */
	public function get_error_files( $per_page = 20, $page_number = 1 )
	{
		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}pwm_akademos_po WHERE `path` LIKE '".$this->errorDir."' AND `status` = 2 ORDER BY `id` DESC";

		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public function record_count() 
	{
		global $wpdb;

		$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}pwm_akademos_po WHERE `path` LIKE '".$this->errorDir."' AND `status` = 2 ORDER BY `id` DESC";

		return $wpdb->get_var( $sql );
	}

	/** 
	 * Text displayed when no error file is available
	 */
	public function no_items()
	{
		_e( 'No error file found.', 'pwm-shipments' );
	}

	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_id
	 *
	 * @return mixed
	 */
	public function column_default( $item, $column_id )
	{
		$file = '';
		if(strtotime('2020-04-01') < strtotime($item['date'])) {
			$file = $this->connection->readFile($item['path'].'shipping'.date('Ymd', strtotime($item['date'])).'.po');
		}

		switch ( $column_id ) {
			case 'name':
				return $item['name'];
			case 'status':
				return $this->statuses[$item['status']];
			case 'totalorders':
				return substr_count( $file, "\n" );
			case 'date':
				return $item['date'];
			default:
				return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}

	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() 
	{
		$columns = array(
			'name'			=> __( 'File Name', 'pwm-shipments' ),
			'status'		=> __( 'Status', 'pwm-shipments' ),
			'totalorders'	=> __( 'Total No of orders', 'pwm-shipments' ),
			// 'errororders'	=> __( 'Error Orders', 'pwm-shipments' ),
			'date'			=> __( 'Date', 'pwm-shipments' ),
			'action'		=> __( 'Action', 'pwm-shipments' ),
		);

		return $columns;
	}
        
	/**
	* Define the sortable columns
	*
	* @return Array
	*/
	public function get_sortable_columns()
	{
		return array('date' => array('date', true));
	}

	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_name( $item ) 
	{
		$title = '<h5><strong>' . $item['name'] . '</strong></h5>';
		$path = '<small><b>Path:</b> ' . $item['path'] . '</small>';

		return $title . $path;
	}

	/**
	 * Method for open po error file action
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_action( $item )
	{
		$open_po_file_nonce = wp_create_nonce( 'open_po_file' );
		if(strtotime('2020-04-01') < strtotime($item['date'])) {
			$filename = 'shipping'.date('Ymd', strtotime($item['date'])).'.po';
			$filedata = array($item['id'], $filename, $item['path']);
			$actions = array(
				'open-file' => sprintf( '<a href="?page=%s&action=%s&filedata=%s&_wpnonce=%s" class="button button-primary" target="_blank">%s</a>', esc_attr( $_REQUEST['page'] ), 'open-file', base64_encode(implode(',', $filedata)), $open_po_file_nonce, __( 'Fix Error Orders', 'pwm-shipments' ) ),
			);
		} else {
			$actions = array(
				'open-file' => sprintf( '<a class="button button-danger" target="_blank">%s</a>', __( 'Outdated File, Can\'t fix!', 'pwm-shipments' ) ),
			);
		}

		$open_po_file = $this->row_actions( $actions, true );

		return $open_po_file;
	}

	/**
	 * Method for file date
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_date( $item )
	{
	?>
		<abbr title="<?php echo date('m-d-Y H:i:s a', strtotime($item['date'])); ?>"><?php echo date('m-d-Y', strtotime($item['date'])); ?></abbr>
	<?php
	}

	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items()
	{
		/** 
		 * Process actions
		 */
		$this->process_actions();

		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);

		$per_page     = $this->get_items_per_page( 'po_error_files_per_page', 15 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( array(
			'total_items' => $total_items, 	//WE have to calculate the total number of items
			'per_page'    => $per_page 		//WE have to determine how many items to show on a page
		) );

		$this->items = self::get_error_files( $per_page, $current_page );
	}

	/**
	 * Handles page actions for open and save error files
	 */
	public function process_actions()
	{
		if ( 'open-file' === $this->current_action() ) {
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, 'open_po_file' ) ) {
				wp_die( 'WP Nonce not verified! Go get a life script kiddies!' );
			}

			$filedata = base64_decode($_GET['filedata']);
			$file = explode(',', $filedata);

			echo '<input type="hidden" name="error_file_id" id="error_file_id" value="'.$file[0].'">
					<input type="hidden" name="error_file_name" id="error_file_name" value="'.$file[1].'">
					<input type="hidden" name="error_file_path" id="error_file_path" value="'.$file[2].'">';

			$order_data = self::processSingleFile($file[2].$file[1]);

			echo $order_data;

			exit;
		}
	}

	/**
	 * Retrieve error files from akademos ftp shipping error folder
	 */
	public function processSingleFile($file, $local = false)
    {
    	$html = '';

        $html .= '<div>
					<a href="?page=pwm-shipments-manual-handle-errors" class="button button-primary">Back to files list</a>
				</div>';

		$html .= '<hr>';

    	if( ! $this->connection->fileExists($file) ) {
    		$html .= '<span><strong>File Path: </strong> '.$file.'</span><br><br>File not found on FTP error folder!';
    		return $html;
    		
    	}

    	$orders = array();

    	if ( $local ) {
            $temp = fopen($file, "r");
        } else {
            $temp = tmpfile();
            $metaDatas = stream_get_meta_data($temp);
            $tmpFilename = $metaDatas['uri'];
            $this->connection->getFile($tmpFilename, $file);
            $fileContents = file_get_contents($tmpFilename);
        }
        $rawLines = explode(PHP_EOL, $fileContents);

        $html .= '<style>
        			tr.errorrow {background-color: #ffbaba !important;}
					#error_file_data .errorcolumn {padding: 10px;}
					#error_file_data .errorcolumn input[type="text"] {width: 100%;height: 30px;outline: #03A9F4 auto 1px;}
				</style>';

		if($rawLines) {
			$rawLines = array_filter($rawLines);
			$html .= '<div style="overflow: auto;margin-bottom: 20px;">
						<button class="button button-primary pull-right" onclick="saveTableData(1)">Save and Move File to done</button>
						<button class="button button-primary pull-right" style="margin: 0 10px;" onclick="saveTableData(0)">Only Save Data</button> 
						<span><strong>File Path: </strong> '.$file.'</span><br>
						<span><strong>Total Number of Orders: </strong> '.count($rawLines).'</span><br>
						<span class="errororderno"><strong>Number of orders has error: </strong> <i></i></span>
				</div>';

			$html .= '<div class="table-responsive"><table id="error_file_data" class="wp-list-table widefat striped">
				<thead>
					<tr>
						<th>'.__( 'Column 1', 'pwm-shipments' ).'</th>
						<th>'.__( 'Column 2', 'pwm-shipments' ).'</th>
						<th>'.__( 'Barcode', 'pwm-shipments' ).'</th>
						<th style="width: 65px !important;">'.__( 'Qty', 'pwm-shipments' ).'</th>
						<th>'.__( 'Name', 'pwm-shipments' ).'</th>
						<th>'.__( 'Address 1', 'pwm-shipments' ).'</th>
						<th>'.__( 'Address 2', 'pwm-shipments' ).'</th>
						<th>'.__( 'City', 'pwm-shipments' ).'</th>
						<th style="width: 80px !important;">'.__( 'State', 'pwm-shipments' ).'</th>
						<th>'.__( 'Zip', 'pwm-shipments' ).'</th>
						<th>'.__( 'Shipping Method', 'pwm-shipments' ).'</th>
						<th style="width: 200px !important;">'.__( 'Tracking Number', 'pwm-shipments' ).'</th>
					</tr>
				</thead>
				<tbody>';
	        while (($data = fgetcsv($temp, 0, "|")) !== FALSE && $data !== null) {
	            if (count($data) < 12) {
	                continue;
	            }
	            $html .= '<tr>';
	            foreach ($data as $key => $value) {
	            	if ($key == 11 && '------' == substr($value, 0, 6)) {
	            		$html .= '<td data-id="'.$key.'" data-value="'.$value.'" class="errorcolumn"><input type="text" title="Click to change the value" /></td>';
	            	} else {
						$html .= '<td data-id="'.$key.'" data-value="'.$value.'" >'.$value.'</td>';
					}
				}
				$html .= '</tr>';
			}
			$html .= '</tbody>
				<tfoot>
					<tr>
						<th>'.__( 'Column 1', 'pwm-shipments' ).'</th>
						<th>'.__( 'Column 2', 'pwm-shipments' ).'</th>
						<th>'.__( 'Barcode', 'pwm-shipments' ).'</th>
						<th style="width: 65px !important;">'.__( 'Qty', 'pwm-shipments' ).'</th>
						<th>'.__( 'Name', 'pwm-shipments' ).'</th>
						<th>'.__( 'Address 1', 'pwm-shipments' ).'</th>
						<th>'.__( 'Address 2', 'pwm-shipments' ).'</th>
						<th>'.__( 'City', 'pwm-shipments' ).'</th>
						<th style="width: 80px !important;">'.__( 'State', 'pwm-shipments' ).'</th>
						<th>'.__( 'Zip', 'pwm-shipments' ).'</th>
						<th>'.__( 'Shipping Method', 'pwm-shipments' ).'</th>
						<th style="width: 200px !important;">'.__( 'Tracking Number', 'pwm-shipments' ).'</th>
					</tr>
				</tfoot>
			</table></div>';

			$html .= '<div style="overflow: auto;margin-top: 20px;">
						<button class="button button-primary pull-right" onclick="saveTableData(1)">Save and Move File to done</button>
						<button class="button button-primary pull-right" style="margin: 0 10px;" onclick="saveTableData(0)">Only Save Data</button> 
						<span><strong>File Path: </strong> '.$file.'</span><br>
						<span><strong>Total Number of Orders: </strong> '.count($rawLines).'</span><br>
						<span class="errororderno"><strong>Number of orders has error: </strong> <i></i></span>
				</div>';
		}

		$html .= '<hr>';

		$html .= '<div>
					<a href="?page=pwm-shipments-manual-handle-errors" class="button button-primary">Back to files list</a>
				</div>';

		$html .= "<script type='text/javascript'>
				jQuery(function($) {
					var errorcolumn = $('.errorcolumn');
					var numErrorColumn = errorcolumn.length;
					$('.errororderno i').text(numErrorColumn);
					errorcolumn.parent().addClass('errorrow');
				});
				function saveTableData(move = 0) {
					var error_file_id = jQuery('#error_file_id').val(),
						error_file_name = jQuery('#error_file_name').val(),
						error_file_path = jQuery('#error_file_path').val();
					var ordersData = [];
					var errortable = jQuery('#error_file_data tbody');
					errortable.find('tr').each(function (i) {
						var tds = jQuery(this).find('td');
						var orderData = [];
						for (i = 0; i < tds.size(); i++) {
							var std = tds.eq(i),
								errorvalue =  std.hasClass('errorcolumn') ? std.find('input').val() : std.data('value');
							orderData.push(errorvalue);
						}
						ordersData.push(orderData.join('|'));
						// console.log(orderData.join('|'));
					});
					// console.log(ordersData);

					var data = {
		                'action'			: 'pwm_shipments_save_error_table_data',
		                'error_file_id'		: error_file_id,
		                'error_file_name'	: error_file_name,
		                'error_file_path'	: error_file_path,
		                'ordersData'		: ordersData,
		                'move'				: move
		            };

		            //ajaxurl is defined by WordPress
            		jQuery.post(ajaxurl, data, ajaxErrorTableSaveCallback);
				}

				function ajaxErrorTableSaveCallback(response_text) {
					console.log(response_text);
					alert(response_text.message+' Click Ok to go back to PO Error Files list!');
					window.location.replace('".get_admin_url()."admin.php?page=pwm-shipments-manual-handle-errors');
				}
			</script>";

		return $html;
	}
}
<?php
// WP_List_Table is not loaded automatically so we need to load it in our application

if( ! class_exists( 'WP_List_Table' ) ) {

    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
/**
 * Create a new shipments monthly report class
 */
if (!class_exists("PWM_Shipments_Inventory")) {

    class PWM_Shipments_Inventory extends WP_List_Table {
        /**
        * Run a constructor to fetch data from the custom database table "wp_pwm_shipping_all_orders".
        *
        * @return Void
        */
        public $data;
        public $statuses = array(

            1 => 'Label Printed',

            2 => 'Active',

            3 => 'Label Error',

        );

        /**
         * Run a constructor.
         *
         * @return Void
         */

        public function __construct() {

            parent::__construct();

            

            $this->data = get_option('pwm-shipping-akademos-inventory-arr');

        }

        /**
         * Report Generator Form.
         *
         * @return Void
         */

        public function create_inventory_form() {

            $nonce = wp_create_nonce("import_nonce");

            $html_form = '<form id="import_csv" class="pwm-form" enctype="multipart/form-data">

                          <p class="import-message"></p>

                          <input type="file" name="import_inventory" id="import_inventory" accept=".csv">

                          <input type="hidden" name="action" value="pwm_action_inventory_import">

                          <input type="hidden" name="import_nonce" id="import_nonce" value="' . $nonce . '">

                          <input type="hidden" name="csv_id" name="csv_id">

                          <input type="submit" name="submit_csv" id="submit_csv" value="Save import" class="btn btn-default form-control"></form>';

            return $html_form;

        }

        

        public function single_entry_form() {

            $html_form = '<form id="add_inventory" enctype="multipart/form-data">

                          <p class="insert-message"></p>

                          <lable>Order ID</lable>

                          <input type="text" name="order_id" id="order_id" value="" required>

                          <lable>Barcode</lable>

                          <input type="text" name="barcode" id="barcode" value="" required>

                          <lable>Price</lable>

                          <input type="text" name="price" id="price" value="" required>

                          <lable>Item Weight</lable>

                          <input type="text" name="weight" id="weight" value="" required>

                          <input type="hidden" name="action" value="pwm_action_add_single_inventory">

                          <input type="submit" name="submit_entry" id="submit_entry" value="Submit"></form>';

            return $html_form;

        }

        

        public function entry_table() {

            $inventoryItems = get_option('pwm-shipping-akademos-inventory-arr');

            $entry_table = '<br><br><table class="wp-list-table widefat fixed striped toplevel_page_pwm-shipments"><tr><th>Sr.No.</th><th>Order ID</th><th>Barcode</th><th>Price</th><th>Weight</th></tr>';

            foreach ($inventoryItems as $ikey => $ival) {

                $entry_table .= '<tr><td>' . ($ikey + 1) . '</td><td>' . $ival[0] . '</td><td>' . $ival[1] . '</td><td>$' . $ival[2] . '</td><td>' . $ival[3] . '</td></tr>';

            }

            $entry_table .= '</table><br><br>';

            return $entry_table;

        }



        public static function import_inventory($csv_file) {

            // Import CSV

            if (isset($csv_file)) {

                // File extension

                $extension = pathinfo($csv_file, PATHINFO_EXTENSION);

                // If file extension is 'csv'

                if (!empty($csv_file) && $extension == 'csv') {

                    $inventoryArr = array();

                    // Open file in read mode

                    $csvFile = fopen($csv_file, 'r');

                    //fgetcsv($csvFile); // Skipping header row

                    $inventoryOption = get_option('pwm-shipping-akademos-inventory-arr');

                    $inventoryOrder = array();

                    foreach ($inventoryOption as $inKey => $inVal) {

                        $inventoryOrder[] = array($inVal[0], $inVal[1], $inVal[2], $inVal[3]);

                    }



                    // Read file

                    while (($csvData = fgetcsv($csvFile)) !== FALSE) {

                        $csvData = array_map("utf8_encode", $csvData);



                        foreach (preg_split('/(\r\n|[\r\n])/', $csvData[0]) as $line) {

                            //split non numeric characters (incase typo) ex 100152:20120152|28.95|2

                            $data = array_map('trim', preg_split("/[^0-9.\s]/", $line));

                            if (count($data) === 4 && is_numeric($data[0]) && is_numeric($data[1]) && is_numeric($data[2]) && is_numeric($data[3])) {

                                //order | barcode | cost | weight

                                $dataArr = array(

                                    intval($data[0]),

                                    intval($data[1]),

                                    floatval($data[2]),

                                    floatval($data[3])

                                );

                                if (!empty($inventoryOrder)) {

                                    foreach ($inventoryOrder as $invKey => $invOval) {

                                        if ($invOval[0] == $data[0]) {

                                            unset($inventoryOrder[$invKey]);

                                        }

                                    }

                                    if (!in_array($dataArr, $inventoryOrder)) {

                                        $inventoryArr[] = $dataArr;

                                    } else {

                                        echo 'Already Exists!';

                                    }

                                } else {

                                    $inventoryArr[] = $dataArr;

                                }

                            }

                        }

                    }

                    $combinedInventory = array_merge($inventoryArr, $inventoryOrder);

                    update_option('pwm-shipping-akademos-inventory-arr', $combinedInventory);



                    wp_send_json_success(array('msg' => 'Inventory successfully imported.'));

                } else {

                    wp_send_json_error(array('msg' => 'Failed to import file data. Please check your file.'));

                }

            }

        }

        

        public static function insert_single_inventory($data) {

            if(is_array($data) && !empty($data)) {

                $inventoryOption = get_option('pwm-shipping-akademos-inventory-arr');

                $inventoryOrder = array();

                foreach ($inventoryOption as $inKey => $inVal) {

                    $inventoryOrder[] = array($inVal[0], $inVal[1], $inVal[2], $inVal[3]);

                }

                if(is_numeric($data['order_id']) && is_numeric($data['barcode']) && is_numeric($data['price']) && is_numeric($data['weight'])) {

                    //order | barcode | cost | weight

                    $dataArr = array(

                        intval($data['order_id']),

                        intval($data['barcode']),

                        floatval($data['price']),

                        floatval($data['weight'])

                    );



                    if (!empty($inventoryOrder)) {

                        foreach ($inventoryOrder as $invKey => $invOval) {

                            if ($invOval[0] == $data['order_id']) {

                                unset($inventoryOrder[$invKey]);

                            }

                        }

                        if (!in_array($dataArr, $inventoryOrder)) {

                            $inventoryArr[] = $dataArr;

                        } else {

                            echo 'Already Exists!';

                        }

                    } else {

                        $inventoryArr[] = $dataArr;

                    }



                    $combinedInventory = array_merge($inventoryArr, $inventoryOrder);

                    update_option('pwm-shipping-akademos-inventory-arr', $combinedInventory);

                    wp_send_json_success(array('msg' => 'Inventory created successfully.'));

                } else {

                    wp_send_json_error(array('msg' => 'Please add only numaric values.'));

                }

            }

        }

        

        /**
        * Prepare the items for the table to process
        *
        * @return Void
        */
        function prepare_items() {

            global $wpdb;
            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $perPage = 20;
            $currentPage = $this->get_pagenum();
            $totalItems = count($this->data);

            $this->set_pagination_args(array(

                'total_items' => $totalItems,

                'per_page' => $perPage

            ));

            usort( $this->data, array( &$this, 'sort_data' ) );
            if( is_array($this->data) ) {
                $data = array_slice($this->data, (($currentPage - 1) * $perPage), $perPage);

            } else {
                $data = array();
            }

            //parse data load specific fields to data
            if(!empty($data)){ $row = array();
                foreach( $data as $index => $row ) {
                    
                    $status = $this->getStatusStr($row['status']);
                    
                    $row['status'] = $status;

                    $row['order_id'] = $row[0];

                    $row['barcode'] = $row[1];

                    $row['price'] = $row[2];

                    $row['weight'] = $row[3];

                    $data[$index] = $row;

                }

                $this->_column_headers = array($columns, $hidden, $sortable);

                $this->items = $data;
            } 


        }

      
        /**
        * Check number of rows in a table
        *
        * @return Array
        */    

        function hasRows() {

           return ( count($this->data) > 0 );

        }

       

        /**
        * Override the parent columns method. Defines the columns to use in your listing table
        *
        * @return Array
        */
        public function get_columns() {

           $columns = array(

               'order_id' => 'Order ID',

               'barcode' => 'Barcode',

               'price' => 'Price',

               'weight' => 'Weight',

           );

           return $columns;

       }

       

       /**
        * Define which columns are hidden
        *
        * @return Array
        */

       public function get_hidden_columns() {

           return array();

       }

       

       /**
        * Define the sortable columns
        *
        * @return Array
        */

        public function get_sortable_columns() {

            return array('order_id' => array('order_id', true));

        }



        /**
         * Define what data to show on each column of the table
         *
         * @param  Array $item        Data
         * @param  String $column_name - Current column name
         *
         * @return Mixed
         */
        function column_default($item, $column) {

            $output = "N/A";

            if( ! empty($item[$column]) ) {

                $output = $item[$column];

            }

            echo $output;

        }



        /**
         * Allows you to sort the data by the variables set in the $_GET
         *
         * @return Mixed
         */

          private function sort_data( $a, $b ) {
            // Set defaults

            $orderby = 'order_id';

            $order = 'asc';

            // If orderby is set, use this as the sort column

            if(!empty($_GET['orderby'])) {

                $orderby = $_GET['orderby'];

            }
            // If order is set use this as the order

            if(!empty($_GET['order'])) {

                $order = $_GET['order'];

            }

            $result = strnatcmp( $a[0], $b[0] );
            return ($order==='asc') ? $result : -$result;

        }



        /**

         * Add actions/links to the columns

         *

         * @return Actions

         */

        function column_id($item) {

            return false;

        }

        /**
        * Render the bulk edit checkbox
        *
        * @param array $item
        *
        * @return string
        */

        function column_cb($item) {

            return sprintf(

                '<input type="checkbox" name="id[]" value="%s" />', $item['id']

            );

        }



        public function getStatusStr($status) {

            return isset($this->statuses[$status]) ? $this->statuses[$status] : 'N/A';

        }

    }

} 
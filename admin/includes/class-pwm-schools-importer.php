<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the functionality to import schools and books from csv.
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin/includes
 * @author     PWM
 */
if( !class_exists("PWM_Import_Schools_Books") ) {
	class PWM_Import_Schools_Books {

		public function __construct() {
			// add_action('admin_menu', array('PWM_Import_Schools_Books', 'admin_menu'));
			// add_action('wp_ajax_pwm-schools-importer-ajax', array('PWM_Import_Schools_Books', 'render_ajax_action'));
		}

		// public static function admin_menu() {
		// 	add_submenu_page( 'pwm-shipments', __( 'Import Schools', 'pwm-schools-importer' ), __( 'Import Schools', 'pwm-schools-importer' ), 'manage_options', 'pwm-schools-importer', array('PWM_Import_Schools_Books', 'render_admin_action'));
		// }

		public static function render_admin_action() {
			$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'upload';
			require_once(PWM_SHIPMENTS_PLUGIN_ROOT_DIR.'/admin/pages/import-steps/pwm-schools-importer-common.php');
			require_once(PWM_SHIPMENTS_PLUGIN_ROOT_DIR."/admin/pages/import-steps/pwm-schools-importer-{$action}.php");
		}

		public static function render_ajax_action() {
			require_once(PWM_SHIPMENTS_PLUGIN_ROOT_DIR."/admin/pages/import-steps/pwm-schools-importer-ajax.php");
			die(); // this is required to return a proper result
		}
	}
}



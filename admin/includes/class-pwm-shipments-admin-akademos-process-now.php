<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the akademos packing list and search by date functionality 
 * It will display revenu and report of books, orders and customers.
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin/includes
 * @author     PWM
 */
if( !class_exists("pwm_shipments_admin_akademos_process_now") ) 
{
    
    class pwm_shipments_admin_akademos_process_now 
    {
    	protected $time, $nextRunSet;

    	protected $dateformat;

        /**
         * Run a constructor.
         *
         * @return Void
        */
        public function __construct()
        {
		    date_default_timezone_set("America/Detroit");
		    $this->dateformat = 'Y-m-d H:i:s';
		    $this->time = time();
		    $this->nextRunSet = strtotime('+15 minutes');
        }

        public function main()
        {
		    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
		    switch($action) {
		        case "fix-labels":
		            $this->fixLabels();
		            break;
		        default:
		            $this->akademosProcessPOs();
		            break;
		    }
        }

        public function akademosProcessPOs()
        {
        	global $wpdb;

        	$this->nextRun = intval(get_option("pwm-akademos-daily-cron-next-run", $time));
	        if( $this->time < $this->nextRun ) {
	            $msg = "Not ready for new cron NOW:$this->time < NEXT-RUN:$this->nextRun \n Next run set $this->nextRunSet";
	            die($msg);
	        }

	        update_option("pwm-akademos-daily-cron-next-run", $this->nextRunSet);
	        set_time_limit(3600); //dont run for over an hour
	        error_reporting(E_ALL);
		    ini_set('display_errors', 1);
		    ignore_user_abort(true); //dont stop after user leaves

		    update_option('pwm-akademos-daily-cron-usps-purchase-count', 0);

		    $helper = new PWM_ShippingAkademosCronHelper();
		    $helper->setFilesToProcess();
		    $count = 0;

		    foreach($helper->filesToProcess as $file) {
		        //validate file
		        $fileName = basename($file);
		        if (strpos(strtolower($fileName), ".po") === FALSE)
		            continue;

		        echo "\n<hr><p>Processing: <b>$fileName</b></p>";
		        echo("Proccessing: $fileName<br>");

		        //do orders
		        $orders = $helper->processSingleFile($file);
		        $contents = "";
		        $hasFileErrored = false;

		        foreach($orders as $ordersToAddress) {
		            $count++;

		            //go through each order to specific address
		            //SUM all book weights, set to -1 if can't find any item in inventory
		            $weight = 0;
		            foreach($ordersToAddress as $order) {
		                if ($order['createLabel'] === false) {
		                    continue;
		                } else if ($order['data']['weight'] > 0.0) {
		                    $weight += $order['data']['weight'];
		                } else if ($order['data']['weight'] == 0.0) {
		                    $weight += $order['data']['weight'];
		                } else {
		                    //can't find in inventory
		                    ob_start();
		                    var_dump($order);
		                    echo("<br>Can't find order weight of: " . ob_get_clean()."<br>");
		                    $weight = -1;
		                    break;
		                }
		            }

		            $trackingNumber = false;
		            $akademosOrderData = array();
		            //generate label once
		            if ($weight > 0) {
		                $shippingMethodRaw = $ordersToAddress[0]['data']['shippingMethod'];
		                $rawInputLine = $ordersToAddress[0]['inputLineFull'];
		                $name = $ordersToAddress[0]['data']['name'];
		                $address1 = $ordersToAddress[0]['data']['address1'];
		                $address2 = $ordersToAddress[0]['data']['address2'];
		                $city = $ordersToAddress[0]['data']['city'];
		                $state = $ordersToAddress[0]['data']['state'];
		                $zip = $ordersToAddress[0]['data']['zip'];

		                try {
			                if (strpos($shippingMethodRaw, "USPS") !== FALSE) {
			                    $shippingMethod = str_replace("USPS-", "US-", $shippingMethodRaw);

			                    $zipAddon = null;
						        if ( strlen($zip) > 5 ) {
						            $zip = substr($zip,0,5);
						            $zipAddon = substr($zip,6);
						        }
						        
						        if ( strlen($name) <= 46 && strlen($address1) <= 46 && strlen($address2) <= 46 && strlen($city) <= 50 && strlen($state) <= 50) {
				                    $PWM_USPS = (new PWM_USPS())
				                    	->setInputFile($fileName)
				                    	->setInputLine($rawInputLine)
				                    	->setServiceType($shippingMethod)
				                    	->setWeightLb($weight)
				                    	->setFullname($name)
				                    	->setAddress1($address1)
				                    	->setAddress2($address2)
				                    	->setCity($city)
				                    	->setState($state)
				                    	->setZipcode($zip)
				                    	->setZIPcodeAddOn($zipAddon)
				                    	->setToAddress()
				                    	->setFromAddress();

				                    list($akademosOrderData, $trackingNumber) = $PWM_USPS->orderData();
				                } else {
				                	$trackingNumber = false;
				                	$akademosOrderData = false;
				                }
			                } else if (strpos($shippingMethodRaw, "UPS") !== FALSE) {
			                    $shippingMethod = str_replace("UPS-", "", $shippingMethodRaw);

			                    $PWM_UPS = (new PWM_UPS())
			                    	->setInputFile($fileName)
			                    	->setInputLine($rawInputLine)
			                    	->setWeight($weight)
			                    	->setFullname($name)
			                    	->setAddress1($address1)
			                    	->setAddress2($address2)
			                    	->setCity($city)
			                    	->setState($state)
			                    	->setZip($zip);

			                    if ($shippingMethod === 'N') {
			                        //see if can  do next day air saver
			                        $PWM_UPS->setShippingMethod('NS');
			                        list($akademosOrderData, $trackingNumber) = $PWM_UPS->createLabel();

			                        //if failed just do next day air
			                        if ($trackingNumber === false) {
				                        $PWM_UPS->setShippingMethod($shippingMethod);
				                        list($akademosOrderData, $trackingNumber) = $PWM_UPS->createLabel();
			                        }
			                    } else {
			                    	$PWM_UPS->setShippingMethod($shippingMethod);
				                    list($akademosOrderData, $trackingNumber) = $PWM_UPS->createLabel();
			                    }
			                } else {
			                    $trackingNumber = false;
			                    $akademosOrderData = false;
			                    $shippingMethod = "Error";
			                }
			            } catch (exception $e) {
			            	echo 'Caught exception: ',  $e->getMessage(), "\n";
			            	$trackingNumber = false;
			                $akademosOrderData = false;
			                $shippingMethod = "Error";
						}
		            }

		            //go through each order and generate PO files with tracking # or error msg
		            foreach($ordersToAddress as $k => $order) {
		                if ($order['createLabel'] === false) {
		                    if( count(explode("|", $order['inputLineFull'])) === 12) {
		                        //already has tracking #
		                        $msg = "";
		                    } else {
		                        $msg = "------LABEL ERROR-----";
		                        $hasFileErrored = true;
		                    }
		                } else if($weight == 0) {
		                    $msg = '';
		                } else if($weight === -1) {
		                    $msg = "------Inventory Lookup ERROR-----";
		                    $hasFileErrored = true;
		                } else if($trackingNumber === false || strlen($trackingNumber) === 0 ) {
		                    $msg = "------LABEL ERROR-----";
		                    $hasFileErrored = true;
		                } else {
		                    $msg = $trackingNumber;
		                }

		                $contents .= $order['inputLineFull'] . "|$msg" . PHP_EOL;

		                if( is_array($akademosOrderData) && !empty($akademosOrderData['response']) ) {
		                    $akademosOrderData['input_line'] = $order['inputLineFull'];
		                    $wpdb->insert( "{$wpdb->prefix}pwm_shipping_akademos_orders", $akademosOrderData);
		                    $typeId = $wpdb->insert_id;
		                    $insertData = array(
		                        'type' => 'akademos',
		                        'type_id' => $typeId,
		                        'status' => 2,
		                        'inventory_id' => $order['data']['bookCode'],
		                        //'created' => "2016-08-25 10:30:00",
		                        'qty' => $order['data']['qty'],
		                    );
		                    $wpdb->insert( "{$wpdb->prefix}pwm_shipping_all_orders", $insertData);
		                    ob_start();
		                    var_dump($typeId, "{$wpdb->prefix}pwm_shipping_akademos_orders", $akademosOrderData, "{$wpdb->prefix}pwm_shipping_all_orders", $insertData);
		                    ob_get_clean();
		                } else {
		                    ob_start();
		                    var_dump("ERROR INSERTING", $k, $akademosOrderData);
		                    ob_get_clean();
		                }
		            }
		        }
		        echo("Done with $file<br>");
		        $helper->savePo($hasFileErrored, $file, $fileName, $contents);
		        echo("<br>Saved $file<br>");
		    }
        }

        public function fixLabels()
        {
        	//if labels img isn't generated correctly fix it
        	if( ! isset($_REQUEST['backed-up-db']) ) die("MUST BACKUP DB BEFORE");

        	echo "<PRE>";
    		ini_set('display_errors', 1);

    		$query = "SELECT * FROM {$wpdb->prefix}pwm_shipping_akademos_orders WHERE `label_base64` IS NULL OR `label_base64` = '' AND `response` != '' ";
    		$hasMore = true;
		    $start = 0;
		    $perPage = 5;
    		while( $hasMore ) {
    			$fullQuery = $query . " LIMIT $start, $perPage";
    			$rows = $wpdb->get_results($query, ARRAY_A);
    			$start += $perPage;

    			echo $fullQuery;

    			if( count($rows) === 0 ) {
		            $hasMore = false;
		            echo "\nNo more rows";
		            continue;
		        }

		        foreach($rows as $i => $row ) {
		            var_dump($i);
		            if( empty($row['response']) ) {
		                var_dump("Row $row[id] has empty response");
		                continue;
		            }
		            $r = unserialize($row['response']);
		            if( $r instanceof stdClass ) {
		                //usps
		                var_dump("USPS EMPTY LABEL $row[input_line]");
		            } else if ( $r instanceof \Awsp\Ship\LabelResponse ) {
		                //ups
		                if( ! is_array($r->labels) || empty($r->labels[0]['label_image']) ) {
		                    var_dump("UPS SKIPPING $row[id]", $r);
		                    continue;
		                }
		                $label = $r->labels[0]['label_image'];
		                $imgRaw = imagecreatefromstring(base64_decode($label));
		                $imgRotated = imagerotate($imgRaw, 270, 0);
		                ob_start();
		                imagegif($imgRotated, null);
		                $gifRotated = ob_get_clean();
		                $row['label_base64'] = base64_encode(pwmConvertGifToZPL($gifRotated));
		                var_dump("UPDATING $row[input_line]", strlen($row['label_base64']));
		                $wpdb->update( "{$wpdb->prefix}pwm_shipping_akademos_orders", $row, array('id' => $row['id']));
		            }
		        }
		    }
        }
    }
}
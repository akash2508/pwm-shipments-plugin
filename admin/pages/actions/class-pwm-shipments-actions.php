<?php
/**
 * The admin-actions related functionality of the plugin.
 *
 * Defines the download label function
 * Defines the delete label/item function
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin/pages/actions
 * @author     PWM
 */
class PWM_Shipments_Actions
{
    public function __construct()
    {
        
        if(isset($_REQUEST['action']) && isset($_REQUEST['page']) && $_REQUEST['page'] == 'pwm-shipments'){
            $action = $_REQUEST['action'];
            switch( $action ) {
                case "delete":
                    $this->pwm_shipments_admin_delete_item();
                break;

                case "download-label":
                    $this->pwm_shipments_admin_download_label_item();
                break;
            }
        }
        if(isset($_REQUEST['action']) && isset($_REQUEST['page']) && $_REQUEST['page'] == 'pwm-shipments-akademos-pos'){
            $action = $_REQUEST['action'];
            switch( $action ) {
                case "delete":
                    $this->pwm_shipments_admin_akademos_pos_delete();
                break;

                case "download":
                    $this->pwm_shipments_admin_akademos_pos_download();
                break;

                case "create_new":
                    $this->pwm_shipments_admin_akademos_pos_create_new_shipping_file();
                break;
            }
        }
        
    }

    /**
     * This function will be called to delete item from pwm shipping all orders and akademos orders
     * If not there was an error
     */
    function pwm_shipments_admin_delete_item()
    {
        $mainId = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : null;
       
        if( $mainId !== null ) {
            global $wpdb;
            $mainQuery = sprintf("SELECT * FROM `wp_pwm_shipping_all_orders` WHERE `id` = %d ",
                    $mainId
            );
            $mainRow = $wpdb->get_row($mainQuery, ARRAY_A);
            $typeTable = sprintf("wp_pwm_shipping_%s_orders", $mainRow['type']);
            $typeWhere = array(
                "id" => $mainRow['type_id'],
            );
            $wpdb->delete($typeTable, $typeWhere);
            
            $mainTable = 'wp_pwm_shipping_all_orders';
            $mainWhere = array(
                "id" => $mainId,
            );
            $wpdb->delete($mainTable, $mainWhere);
        }
        
        $redirectUrl = admin_url('admin.php?page=pwm-shipments');
        pwmWPRedirct($redirectUrl);
        exit();
    }

    /**
     * This function will be called to download label item
     * If not there was an error
     */
    function pwm_shipments_admin_download_label_item()
    {
        global $wpdb;
        $mainId = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : null;
        
        if( $mainId !== null ) {
            $data = array(
                'status' => 'Label Error',
            );
            $where = array(
                'id' => $mainId,
            );
            $wpdb->update($wpdb->prefix.'pwm_shipping_all_orders', $data, $where);
        }else{
            ?>
                <p>Sorry that label doesn't exist</p>
                <script type="text/javascript">
                    alert("Sorry that label doesn't exist");
                    window.history.back();
                </script>
            <?php
            
        }

        if ( $_REQUEST['page'] === 'pwm-shipments' && $_REQUEST['action'] === 'download-label' ) {
            global $wpdb;
            $errored = false;
            $mainId = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : null;
            $fileContents = "";

            if( $mainId !== null ) {
                $mainQuery = sprintf("SELECT * FROM `{$wpdb->prefix}pwm_shipping_all_orders` WHERE `id` = %d ",
                    $mainId
                );
                $mainRow = $wpdb->get_row($mainQuery, ARRAY_A);

                $itemQuery = sprintf("SELECT * FROM `{$wpdb->prefix}pwm_shipping_%s_orders` WHERE `id` = %d ",
                    $mainRow['type'], $mainRow['type_id']
                );
                
                $itemRow = $wpdb->get_row($itemQuery, ARRAY_A);
                
                $fileName = sprintf("label-%s-%s.%s", $itemRow['tracking'], $mainRow['id'], $itemRow['label_type']);
                if( strlen($itemRow['label_link']) > 0 ) {
                    $fileContents = file_get_contents($itemRow['label_link']);
                } else {
                    $fileContents = base64_decode($itemRow['label_base64']);
                }
            } else {
                $errored = true;
            }
           
            if( $errored === false && strlen($fileContents) > 0 ) {    
                $data = array(
                    'status' => 1,//'Label Printed',
                );
                $where = array(
                    'id' => $mainId,
                );
                $wpdb->update($wpdb->prefix.'pwm_shipping_all_orders', $data, $where);
                
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header('Content-Description: File Transfer');
                header("Content-type: text/plain");
                header("Content-Disposition: attachment; filename={$fileName}");
                header("Expires: 0");
                header("Pragma: public");

                $fh = @fopen( 'php://output', 'w' );
                fwrite($fh, $fileContents);
                fclose($fh);
                die;
            }
        }
    }
    /**
     * This function will be called to download Akademos label file
     * If not there was an error
     */
    function pwm_shipments_admin_akademos_pos_download()
    { 
        if( !empty($_GET['page']) && !empty($_GET['action']) && !empty($_GET['id']) 
                && $_GET['page'] === 'pwm-shipments-akademos-pos' && $_GET['action'] === 'download' ) {
            global $wpdb;
            
            $connector = new PWMFtpSiteConnector(PWM_SHIPMENTS_AKADEMOS_FTP_HOST, PWM_SHIPMENTS_AKADEMOS_FTP_USER, PWM_SHIPMENTS_AKADEMOS_FTP_PASS, PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR);
            
            $id = intval($_GET['id']);
            
            $row = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}pwm_akademos_po` WHERE `id` = $id", ARRAY_A);
            
            if( is_array($row) && count($row) > 0 ) {
            
                $filePath = pwmWPForceTrailingSlash($row['path']) . 'shipping'.date('Ymd', strtotime($row['date'])).'.po';
            
                if( $connector->fileExists($filePath) ) {
                    
                    $contents = $connector->readFile($filePath);
             
                    header("Content-Type: text/plain");
                    
                    header('Content-Disposition: attachment; filename="'.$row['name'].'"');
                    
                    header("Content-Length: " . strlen($contents));
                    
                    die($contents);
               
                }else{
               
                    $errors = '<div class="notice notice-error is-dismissible"><p>Sorry, file is not exist to process.</p></div>';
                    echo $errors;
                }
            }
        }
    }
    /**
     * Create new shipping file from the order file.
     * @return [type] [description]
     */
    function pwm_shipments_admin_akademos_pos_create_new_shipping_file()
    {
        if( !empty($_GET['page']) && !empty($_GET['action']) && !empty($_GET['id']) && $_GET['page'] === 'pwm-shipments-akademos-pos' && $_GET['action'] === 'create_new' ) {
            global $wpdb;
            
            $connector = new PWMFtpSiteConnector(PWM_SHIPMENTS_AKADEMOS_FTP_HOST, PWM_SHIPMENTS_AKADEMOS_FTP_USER, PWM_SHIPMENTS_AKADEMOS_FTP_PASS, PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR);

            $id = intval($_GET['id']);
            
            $row = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}pwm_akademos_po` WHERE `id` = $id", ARRAY_A);

            if( is_array($row) && count($row) > 0 ) {

                $orderFilePath = $row['po_name'];
                $shippingFilePath = pwmWPForceTrailingSlash($row['path']) . 'shipping'.date('Ymd', strtotime($row['date'])).'.po';
                $temp = tmpfile();
                $metaDatas = stream_get_meta_data($temp);
                $tmpFilename = $metaDatas['uri'];
                $connector->getFile($tmpFilename, $orderFilePath);
                $fileContents = file_get_contents($tmpFilename);
                $rawLines = explode(PHP_EOL, $fileContents);
                $contents = "";

                while (($data = fgetcsv($temp, 0, "|")) !== FALSE && $data !== null) {
                    if (count($data) < 11) {
                        continue;
                    }
                    
                    $rawInputLine = implode("|", $data);

                    $query = "SELECT * FROM {$wpdb->prefix}pwm_shipping_akademos_orders WHERE `input_line` LIKE '".addslashes($rawInputLine)."' AND `input_file` LIKE '".$row['name']."' AND `created` > '".date("Y-m-d", strtotime($row['date']))." 00:00:00' AND `created` < '".date("Y-m-d", strtotime($row['date']))." 23:59:59' AND response != ''";
                    $results = $wpdb->get_row($query);

                    if( count($results) < 0 ) {
                        $contents .= $results->input_line . "|------LABEL ERROR-----". PHP_EOL;
                    } else {
                        $contents .= $results->input_line . "|".$results->tracking. PHP_EOL;
                    }
                }
                if( ! $connector->fileExists($shippingFilePath) ) {
                    $connector->write($shippingFilePath, $contents, false);
                } else {
                    $shippingFilePath = pwmWPForceTrailingSlash($row['path']) . 'shipping'.date('Ymd', strtotime($row['date'])).'-1.po';
                    $connector->write($shippingFilePath, $contents, false);
                }
             
                header("Content-Type: text/plain");
                    
                header('Content-Disposition: attachment; filename="'.$row['name'].'"');
                    
                header("Content-Length: " . strlen($contents));
                    
                die($contents);
            }
        }

    }

    function pwm_shipments_admin_akademos_pos_delete()
    {
        global $wpdb;
        $id = intval($_REQUEST['id']);
        $wpdb->delete($wpdb->prefix.'pwm_akademos_po', array('id' => $id));
        
        $location = admin_url("admin.php?page=pwm-shipments-akademos-pos");
        pwmWPRedirct($location);
        die;//explicit die
    }
    
    /**
     * This action is to save error table data on akademos FTP
     * 
     */
    public function pwm_shipments_save_error_table_data()
    {
        //return if not the rite page
        if( $_POST['action'] !== "pwm_shipments_save_error_table_data" || empty($_POST['error_file_id']) ) 
            return;

        global $wpdb;

        header("Content-type: text/plain");

        $file_id = intval($_POST['error_file_id']);
        $file_name = sanitize_file_name($_POST['error_file_name']);
        $file_path = sanitize_text_field($_POST['error_file_path']);
        $ordersData = $_POST['ordersData'];
        $move = intval($_POST['move']);

        $file_data = implode(PHP_EOL, $ordersData);

        $connection = new PWMFtpSiteConnector(PWM_SHIPMENTS_AKADEMOS_FTP_HOST, PWM_SHIPMENTS_AKADEMOS_FTP_USER, PWM_SHIPMENTS_AKADEMOS_FTP_PASS, PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR);

        $shippingDir = '/shipping/';

        $outputFileName = $shippingDir . 'error/' . $file_name;

        $moveTempFileName = $shippingDir . 'temp/' . $file_name;
        $moveDoneFileName = $shippingDir . 'done/' . $file_name;

        //TODO if file exists append contents
        if( $connection->fileExists($outputFileName) ) {
            $connection->write($outputFileName, $file_data, false);
            $message = 'Success: File updated successfully!';
            if($move) {
                $connection->moveFile($outputFileName, $moveTempFileName);
                $connection->moveFile($outputFileName, $moveDoneFileName);

                $rowData = array(
                    'id'        => $file_id,
                    'status'    => 1,
                    'path'      => $shippingDir . 'temp/',
                    'date'      => time(),

                );

                $wpdb->update( "{$wpdb->prefix}pwm_akademos_po", $rowData, array('id' => $file_id));

                $message = 'Success: File moved successfully! New file path: '.$moveTempFileName;
            }
        } else {
            $message = 'Error: File not found on FTP error folder!';
        }

        echo wp_send_json(array(
            'file_id' => $file_id,
            'file_name' => $file_name,
            'file_path' => $file_path,
            'outputFileName' => $outputFileName,
            'ordersData' => $file_data,
            'message' => $message,
        ));

        wp_die();
    }


    /**
     * This function will be called to allow monthly report download from Reports page
     * If not there was an error
     */
    public function pwm_shipments_admin_monthly_report_download()
    {
        //return if not the rite page
        if( $_REQUEST['action'] !== "pwm_shipments_monthly_report" || empty($_REQUEST['pwm-date']) ) 
            return;
        
        if( !headers_sent() && isset($_REQUEST['pwm-date']) ) {
            $time = $_REQUEST['pwm-date'];
            PWM_Shipments_Admin_Monthly_Report::wirte_pdf($time);
        } else {
            echo "<h1>Error Downloading Report</h1>";
        }
        die;
    }
    
    /**
     * This action is to import inventory csv
     * 
     */
    public function pwm_action_inventory_import()
    {
        $csv_file = get_attached_file($_POST['csv_id']);
        PWM_Shipments_Inventory::import_inventory($csv_file);
    }
    
    /**
     * This action is to import schools and books csv
     * 
     */
    public function pwm_schools_importer_ajax()
    {
        PWM_Import_Schools_Books::render_ajax_action();
    }
    
    /**
     * This action is to insert single inventory
     * 
     */
    public function pwm_action_add_single_inventory()
    {
        PWM_Shipments_Inventory::insert_single_inventory($_POST);
    }

    /**
     * Listen before headers
     * create zip of all labels
     ** @packing list module start here
     ** Download packing list in a pdf file
     * mark all woo orders as processed
     */
    public function pwm_shipments_admin_akademos_packing_list_pdf($poFile, array $poTable, $totalsContent)
    {
        pwm_shipments_tcpdf_packing_list_class();
        $pdf = new PWM_Shipments_TCPDF_Packing_List('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Pro Web Marketing');
        $pdf->SetTitle($poFile);
        $pdf->SetSubject('Akademos Shipping Labels');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set font
        $pdf->SetFont('times', '', 9);

        // add a page
        $rowCount = count($poTable['rows']);
        $innerTableContents = "";
        for( $i = 0; $i < $rowCount; $i++) {
            $innerTableContents .= $poTable['rows'][$i];
            // is 8 rows long or last section of table
            if( ($i !== 0 && ($i % 14) === 0)
                    || ($i + 1) === $rowCount ) {
                $pageContents = $poTable['open'] . $poTable['headings'] . $innerTableContents . $poTable['close'];

                $pdf->AddPage();
                $pdf->SetY(15);
                $pdf->writeHTML($pageContents);
                $innerTableContents = "";
            }
        }

        $pdf->AddPage();
        $pdf->SetFont('times', '', 10);
        $pdf->writeHTML($totalsContent);

        //save to file
        $pdf->Output($poFile, 'F');
    }

    public function pwm_shipments_admin_akademos_create_woo_order(stdClass $order)
    {
        global $wpdb;
        $akademosPlaceholderProductId = 653;
        $wcOrder = wc_create_order();
        $wcOrder->add_product(get_product($akademosPlaceholderProductId), 1);
        $data = explode("|", $order->input_line);
        if (count($data) > 9 && $wcOrder instanceof WC_Order) {
            $name = explode(" ", $data[4]);
            $first = isset($name[0]) ? $name[0] : "";
            $last = isset($name[1]) ? $name[1] : "";
            $address1 = $data[5];
            $address2 = $data[6];
            $city = $data[7];
            $state = $data[8];
            $zip = $data[9];
            $country = "";
            $address = array(
                'first_name' => $first,
                'last_name' => $last,
                'company' => "",
                'address_1' => $address1,
                'address_2' => $address2,
                'city' => $city,
                'state' => $state,
                'postcode' => $zip,
                'country' => $country,
            );

            $wcOrder->set_address($address, 'billing');
            $wcOrder->set_address($address, 'shipping');

            $wcOrder->calculate_totals();
            $wcOrder->payment_complete();

            $status = "processing"; // "wc-shipped-shipment";
            $note = "Akademos Order";
            $wcOrder->update_status($status, $note);

            $updateArgs = array('woo_order_id' => $wcOrder->id, 'status' => 1,);
            $where = array('id' => $order->id,);
            $wpdb->update($wpdb->prefix.'pwm_shipping_all_orders', $updateArgs, $where);
            update_post_meta($wcOrder->id, "tracking_number", $order->tracking);
        }
    }

    public function pwm_shipments_download_packinglist_action()
    {
        if ((isset($_REQUEST['page']) && $_REQUEST['page']=== 'pwm-shipments-akademos-packing-list') && isset($_POST['download-now']) && isset($_POST['query'])) {
            global $wpdb;
            $query = base64_decode($_POST['query']);
            $poFile = PWM_SHIPMENTS_PLUGIN_TMP_DIR . date("Y-m-d_") . uniqid() . '_po.pdf';
            $this->pwm_shipments_admin_akademos_packing_list_pdf(
                    $poFile,
                    unserialize(base64_decode($_POST['po-table'])),
                    base64_decode($_POST['totals'])
            );
            
            $orders = $wpdb->get_results($query);
            $labels = array();
            $allTrackingNumbers = array();
            foreach ($orders as $order) {
                //dont printout more than one label to a tracking number 
                //ex: 2 orders to 1 address only needs one label
                if( in_array($order->tracking, $allTrackingNumbers) ) continue;
                $allTrackingNumbers[] = $order->tracking;
                
                $labelFileName = sprintf("%slabel-%s-%s.%s",
                        PWM_SHIPMENTS_PLUGIN_TMP_DIR, $order->id,
                        $order->tracking, $order->label_type
                );
                $labels[] = $labelFileName;
                $contents = base64_decode($order->label_base64);

                file_put_contents($labelFileName, $contents);

                if ( $_REQUEST['pwm-test'] !== 'TRUE' ) {
                    //generate WooCommerce order if not exist already
                    if ($order->woo_order_id === null && ! is_numeric($order->woo_order_id)) {
                        $this->pwm_shipments_admin_akademos_create_woo_order($order);
                    }
                    $wpdb->update('wp_pwm_shipping_all_orders', ['status' => 1], ['id' => $order->id]);
                }
            }
            $zipFiles = array(
                'labels' => $labels,
                $poFile,
            );
            pwm_shipments_general_download_all_files($zipFiles, true);
            die; //explicit die 
        }
    }  

}
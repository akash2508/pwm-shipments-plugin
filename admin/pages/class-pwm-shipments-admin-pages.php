<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin/pages
 * @author     PWM
 */
class PWM_Shipments_Admin_Pages
{

    private $plugin_name;
    
    private $version;

    /**
    * Initialize the class and set its properties.
    *
    * @since    1.0.0
    * @param      string    $plugin_name       The name of this plugin.
    * @param      string    $version    The version of this plugin.
    */
    public function __construct($plugin_name = null, $version = null)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
    * Display the list of shipments
    *
    * @return Void
    */
    public function pwm_shipments_admin_dashboard()
    {
        if ( current_user_can( 'manage_woocommerce' ) ) {
            $pwmShipmentTable = new PWM_Shipments_List_Table();
            ?>
                <div class="wrap pwm-shipments-table">
                    <div id="icon-users" class="icon32"></div>
                    <h1>All Shipments</h1>
                    <div class="content">
                        <form method="get">
                            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                            <?php 
                                $pwmShipmentTable->prepare_items();
                                $pwmShipmentTable->display();
                            ?>
                        </form>
                    </div>
                </div>
            <?php
        }
    }

    /**
    * Display the list of Akademos PO files
    *
    * @return Void
    */
    public function pwm_shipments_admin_akademos_pos()
    {
        if ( current_user_can( 'manage_woocommerce' ) ) {
            $pwmShipmentAkademosTable = new PWM_Shipments_Akademos_POfiles();
            ?>
                <div class="wrap pwm-shipments-table">
                    <div id="icon-users" class="icon32"></div>
                    <h1>Akademos PO Files</h1>
                    <div class="content">
                        <form method="get">
                            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                            <?php 
                                $pwmShipmentAkademosTable->prepare_items();
                                $pwmShipmentAkademosTable->display();
                            ?>
                        </form>
                    </div>
                </div>
            <?php
        }
    }

    /**
    * It will run the cron to process all akademos orders
    *
    * @return Void
    */
    public function pwm_shipments_admin_akademos_process_now()
    {
        $apiUrl = get_admin_url().'admin.php?page=pwm-shipments-akademos-process-now&runcron=true&password=^5ZT5rtrH-DD&time='.time();

        //check if correct authorization
        if ( ! isset($_REQUEST['runcron']) || $_REQUEST['runcron'] === false ) {
        ?>
            <script type="text/javascript">
                window.addEventListener('load', function(){
                    var win = window.open("<?=$apiUrl?>", '_blank');
                    console.log(win);
                    win.focus();
                });
            </script>
            <br/>
            <p>
                If a new tab doesn't open automatically click <a href="<?=$apiUrl?>" target="_blank">HERE</a>
            </p>
        <?php
        } else {

            //check if correct authorization
            if ( ! isset($_REQUEST['password']) || $_REQUEST['password'] !== '^5ZT5rtrH-DD' ) {
                die("Sorry you don't have access");
            }

            $process_now = new pwm_shipments_admin_akademos_process_now();

            $process_now->main();
        }
    }

    /**
    * Handle errors manually in po file orders
    *
    * @return Void
    */
    public function pwm_shipments_manual_handle_errors()
    {
        $errors_handle_table = new PWM_Shipments_Order_Errors_Table();
        ?>
        <div class="wrap pwm-shipments-table">
            <h1><?php echo __( 'All PO Error Files', 'pwm-shipments' ) ?></h1><br>
            <div class="content">
                <form method="get">
                    <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                    <?php 
                        $errors_handle_table->prepare_items();
                        $errors_handle_table->display();
                    ?>
                </form>
            </div>
        </div>
        <?php
    }

    /**
    * We will display packing list items and search by month
    * It will display orders by month and their details
    *
    * @return Void
    */
    public function pwm_shipments_admin_akademos_packing_list()
    {
        $packing_list = new pwm_shipments_admin_akademos_packing_list();
        ?>    
        <div class="wrap pwm-shipments-div">    
            <div id="icon-users" class="icon32"></div>
            <h2>Packing List</h2>
            <div class="packinglist-form border-box">
                <?php echo $packing_list->pwm_shipments_packing_list_form(); ?>
            </div>
        </div>
    <?php
    }

    /**
    * Add/Import Inventory List
    *
    * @return Void
    */
    public function pwm_shipments_admin_inventory()
    {
        if ( current_user_can( 'manage_woocommerce' ) ) {
            $pwmShipmentInventory = new PWM_Shipments_Inventory();
            ?>
                <div class="wrap pwm-shipments-div">
                    <div id="icon-users" class="icon32"></div>
                    <h2>Inventory</h2>

                    <div class="import-inventory border-box">
                        <h4>Import Inventory via CSV file</h4>
                        <p> Please use csv format for import.<br>
                        CSV formate: order # | barcode # | akademos cost | weight per book (in lbs) </p>
                        <span>Sample format: <a href="<?php echo plugins_url().'/pwm-shipments/admin/assets/sample-csv/sample.csv'; ?>" download>sample.csv</a></span>
                        <?php echo $pwmShipmentInventory->create_inventory_form(); ?>
                    </div>
                    <div class="inventory-table">
                        <form method="get">
                            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                            <?php 
                                $pwmShipmentInventory->prepare_items();
                                $pwmShipmentInventory->display();
                            ?>
                        </form>
                    </div>  
                </div>
            <?php
        }
    }

    /**
    * Generage PDF for monthly report
    *
    * @return Void
    */
    public function pwm_shipments_admin_monthly_report()
    {
        if ( current_user_can( 'manage_woocommerce' ) ) {
            $pwmShipmentAdminMonthlyReport = new PWM_Shipments_Admin_Monthly_Report();
            ?>
                <div class="wrap pwm-shipments-table">
                    <div id="icon-users" class="icon32"></div>
                    <h2>Reports</h2>
                    <?php echo $pwmShipmentAdminMonthlyReport->report_form(); ?>
                </div>
            <?php
        }
    }

    /**
    * Import CSV of Schools and Books
    *
    * @return Void
    */
    public function pwm_import_schools_books()
    {
        if ( current_user_can( 'manage_woocommerce' ) ) {
            $PWM_Import_Schools_Books = new PWM_Import_Schools_Books();
            $PWM_Import_Schools_Books->render_admin_action();
        }
    }

}
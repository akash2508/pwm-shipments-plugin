<?php 

    ini_set("auto_detect_line_endings", true);

    $post_data = array(
        'uploaded_file_path' => $_POST['uploaded_file_path'],
        'header_row' => $_POST['header_row'],
        'limit' => $_POST['limit'],
        'offset' => $_POST['offset'],
        'map_to' => maybe_unserialize(stripslashes($_POST['map_to'])),
        'import_csv_separator' => maybe_unserialize(stripslashes($_POST['import_csv_separator'])),
    );

    if(isset($post_data['uploaded_file_path'])) {

        $error_messages = array();

        //now that we have the file, grab contents
        $temp_file_path = $post_data['uploaded_file_path'];
        $handle = fopen( $temp_file_path, 'r' );
        $import_data = array();

        if ( $handle !== FALSE ) {
            while ( ( $line = fgetcsv($handle, 0, $post_data['import_csv_separator']) ) !== FALSE ) {
                $import_data[] = $line;
            }
            fclose( $handle );
        } else {
            $error_messages[] = __( 'Could not open CSV file.', 'pwm-schools-importer' );
        }

        if(sizeof($import_data) == 0) {
            $error_messages[] = __( 'No data found in CSV file.', 'pwm-schools-importer' );
        }

        //discard header row from data set, if we have one
        if(intval($post_data['header_row']) == 1) array_shift($import_data);

        //total size of data to import (not just what we're doing on this pass)
        $row_count = sizeof($import_data);

        //slice down our data based on limit and offset params
        $limit = intval($post_data['limit']);
        $offset = intval($post_data['offset']);
        if($limit > 0 || $offset > 0) {
            $import_data = array_slice($import_data, $offset , ($limit > 0 ? $limit : null), true);
        }

        //a few stats about the current operation to send back to the browser.
        $rows_remaining = ($row_count - ($offset + $limit)) > 0 ? ($row_count - ($offset + $limit)) : 0;
        $insert_count = ($row_count - $rows_remaining);
        $insert_percent = number_format(($insert_count / $row_count) * 100, 1);

        //array that will be sent back to the browser with info about what we inserted.
        $inserted_rows = array();

        // lookup existing product attributes
        $attribute_taxonomies = wc_get_attribute_taxonomies(); 
        if (! is_array($attribute_taxonomies)) $attribute_taxonomies = array();

        //this is where the fun begins
        foreach($import_data as $row_id => $row) {

            //unset new_post_id
            $new_post_id = null;

            //array of imported post data
            $new_post = array();

            //set some defaults in case the post doesn't exist
            $new_post_defaults = array();
            $new_post_defaults['post_type'] = 'product';
            $new_post_defaults['post_status'] = 'publish';
            $new_post_defaults['post_title'] = '';
            $new_post_defaults['post_content'] = '';
            $new_post_defaults['menu_order'] = 0;

            //array of imported post_meta
            $new_post_meta = array();

            //default post_meta to use if the post doesn't exist
            $new_post_meta_defaults = array();
            $new_post_meta_defaults['_visibility'] = 'visible';
            $new_post_meta_defaults['_featured'] = 'no';
            $new_post_meta_defaults['_weight'] = 0;
            $new_post_meta_defaults['_length'] = 0;
            $new_post_meta_defaults['_width'] = 0;
            $new_post_meta_defaults['_height'] = 0;
            $new_post_meta_defaults['_sku'] = '';
            $new_post_meta_defaults['_stock'] = '';
            $new_post_meta_defaults['_sale_price'] = '';
            $new_post_meta_defaults['_sale_price_dates_from'] = '';
            $new_post_meta_defaults['_sale_price_dates_to'] = '';
            $new_post_meta_defaults['_tax_status'] = 'taxable';
            $new_post_meta_defaults['_tax_class'] = '';
            $new_post_meta_defaults['_purchase_note'] = '';
            $new_post_meta_defaults['_downloadable'] = 'no';
            $new_post_meta_defaults['_virtual'] = 'no';
            $new_post_meta_defaults['_backorders'] = 'no';

            //stores tax and term ids so we can associate our product with terms and taxonomies
            //this is a multidimensional array
            //format is: array( 'tax_name' => array(1, 3, 4), 'another_tax_name' => array(5, 9, 23) )
            $new_post_term = '';

            //a list of image URLs to be downloaded.
            $new_post_image_urls = array();

            $new_post_image_paths = array();

            //keep track of any errors or messages generated during post insert or image downloads.
            $new_post_errors = array();
            $new_post_messages = array();

            //track whether or not the post was actually inserted.
            $new_post_insert_success = false;

            foreach($row as $key => $col) {
                $map_to = $post_data['map_to'][$key];

                //skip if the column is blank.
                //useful when two CSV cols are mapped to the same product field.
                //you would do this to merge two columns in your CSV into one product field.
                if(strlen($col) == 0) {
                    continue;
                }

                //prepare the col value for insertion into the database
                switch($map_to) {

                    //post fields
                    case 'post_title':
                    case 'post_content':
                    case 'post_excerpt':
                    case 'post_status':
                    case 'comment_status':
                    case 'ping_status':
                        $new_post[$map_to] = $col;
                        break;

                    //float postmeta fields
                    case '_weight':
                    case '_regular_price':
                        //remove any non-numeric chars except for '.'
                        $col_value = preg_replace("/[^0-9.]/", "", $col);
                        if($col_value == "") break;

                        $new_post_meta[$map_to] = $col_value;
                        break;

                    case 'product_cat_by_name':
                        $tax = str_replace('_by_name', '', $map_to);

                        $term_name = $col;

                        $term_id = '';

                        $term = term_exists($term_name, $tax);

                        //if term does not exist, try to insert it.
                        if( $term === false || $term === 0 || $term === null) {
                            $term = wp_insert_term($term_name, $tax);
                            delete_option("{$tax}_children");
                        }

                        if(is_array($term)) {
                            $term_id = intval($term['term_id']);
                        } else {
                            //uh oh.
                            $new_post_errors[] = "Couldn't find or create {$tax} with path {$term_name}.";
                        }

                        //if we got a term at the end of the path, save the id so we can associate
                        $new_post_term = $term_id;

                        break;

                    case 'product_image_by_url':
                        $image_urls = array('book' => $col);
                        $new_post_image_urls = array_merge($new_post_image_urls, $image_urls);

                        break;

                    case 'product_cat_description':

                        $tax = str_replace('_description', '', $map_to);
                        $term_description = $col;
                        $insert_term_args = array('description' => $term_description);
                        wp_update_term( $new_post_term, $tax, $insert_term_args);

                        break;

                    case 'product_cat_image':
                        $image_urls = array('school' => $col);
                        $new_post_image_urls = array_merge($new_post_image_urls, $image_urls);

                        break;

                }
            }

            //set price to sale price if we have one, regular price otherwise
            $new_post_meta['_price'] = array_key_exists('_sale_price', $new_post_meta) ? $new_post_meta['_sale_price'] : $new_post_meta['_regular_price'];

            //try to find a product with a matching SKU
            $existing_product = null;
            if(array_key_exists('post_title', $new_post) && !empty($new_post['post_title']) > 0) {
                $existing_post_query = array(
                    'numberposts' => 1,
                    's' => $new_post['post_title'],
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'term_id',
                            'terms'    => $new_post_term,
                        ),
                    ),
                    'post_type' => 'product');
                $existing_posts = get_posts($existing_post_query);
                if(is_array($existing_posts) && sizeof($existing_posts) > 0) {
                    $existing_product = array_shift($existing_posts);
                }
            }

            if(strlen($new_post['post_title']) > 0 || $existing_product !== null) {

                //insert/update product
                if($existing_product !== null) {
                    $new_post_messages[] = sprintf( __( 'Updated book data with ID %s.', 'pwm-schools-importer' ), $existing_product->ID );

                    $new_post['ID'] = $existing_product->ID;
                    $new_post_id = wp_update_post($new_post);
                } else {

                    //merge in default values since we're creating a new product from scratch
                    $new_post = array_merge($new_post_defaults, $new_post);
                    $new_post_meta = array_merge($new_post_meta_defaults, $new_post_meta);

                    $new_post_id = wp_insert_post($new_post, true);
                }

                if(is_wp_error($new_post_id)) {
                    $new_post_errors[] = sprintf( __( 'Couldn\'t insert product with name %s.', 'pwm-schools-importer' ), $new_post['post_title'] );
                } elseif($new_post_id == 0) {
                    $new_post_errors[] = sprintf( __( 'Couldn\'t update product with ID %s.', 'pwm-schools-importer' ), $new_post['ID'] );
                } else {
                    //insert successful!
                    $new_post_insert_success = true;

                    //set post_meta on inserted post
                    foreach($new_post_meta as $meta_key => $meta_value) {
                        add_post_meta($new_post_id, $meta_key, $meta_value, true) or
                            update_post_meta($new_post_id, $meta_key, $meta_value);
                    }

                    //set post terms on inserted post
                    wp_set_object_terms($new_post_id, $new_post_term, $tax);

                    //figure out where the uploads folder lives
                    $wp_upload_dir = wp_upload_dir();

                    //grab product images
                    foreach($new_post_image_urls as $image_type => $image_url) {

                        //convert space chars into their hex equivalent.
                        $image_url = str_replace(' ', '%20', trim($image_url));

                        //do some parsing on the image url so we can take a look at
                        //its file extension and file name
                        $parsed_url = parse_url($image_url);
                        $pathinfo = pathinfo($parsed_url['path']);

                        //If our 'image' file doesn't have an image file extension, skip it.
                        $allowed_extensions = array('jpg', 'jpeg', 'gif', 'png');
                        $image_ext = strtolower($pathinfo['extension']);
                        if(!in_array($image_ext, $allowed_extensions)) {
                            $new_post_errors[] = sprintf( __( 'A valid file extension wasn\'t found in %s. Extension found was %s. Allowed extensions are: %s.', 'pwm-schools-importer' ), $image_url, $image_ext, implode( ',', $allowed_extensions ) );
                            continue;
                        }

                        //figure out where we're putting this thing.
                        $dest_filename = wp_unique_filename( $wp_upload_dir['path'], $pathinfo['basename'] );
                        $dest_path = $wp_upload_dir['path'] . '/' . $dest_filename;
                        $dest_url = $wp_upload_dir['url'] . '/' . $dest_filename;

                        //download the image to our local server.
                        // if allow_url_fopen is enabled, we'll use that. Otherwise, we'll try cURL
                        if(ini_get('allow_url_fopen')) {
                            //attempt to copy() file show error on failure.
                            if( ! @copy($image_url, $dest_path)) {
                                $http_status = $http_response_header[0];
                                $new_post_errors[] = sprintf( __( '%s encountered while attempting to download %s', 'pwm-schools-importer' ), $http_status, $image_url );
                            }

                        } elseif(function_exists('curl_init')) {
                            $ch = curl_init($image_url);
                            $fp = fopen($dest_path, "wb");

                            $options = array(
                                CURLOPT_FILE => $fp,
                                CURLOPT_HEADER => 0,
                                CURLOPT_FOLLOWLOCATION => 1,
                                CURLOPT_TIMEOUT => 60); // in seconds

                            curl_setopt_array($ch, $options);
                            curl_exec($ch);
                            $http_status = intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));
                            curl_close($ch);
                            fclose($fp);

                            //delete the file if the download was unsuccessful
                            if($http_status != 200) {
                                unlink($dest_path);
                                $new_post_errors[] = sprintf( __( 'HTTP status %s encountered while attempting to download %s', 'pwm-schools-importer' ), $http_status, $image_url );
                            }
                        } else {
                            //well, damn. no joy, as they say.
                            $error_messages[] = sprintf( __( 'Looks like %s is off and %s is not enabled. No images were imported.', 'pwm-schools-importer' ), '<code>allow_url_fopen</code>', '<code>cURL</code>'  );
                            break;
                        }

                        //make sure we actually got the file.
                        if(!file_exists($dest_path)) {
                            $new_post_errors[] = sprintf( __( 'Couldn\'t download file %s.', 'pwm-schools-importer' ), $image_url );
                            continue;
                        }

                        //whew. are we there yet?
                        $new_post_image_paths[] = array(
                            'type' => $image_type,
                            'path' => $dest_path,
                            'source' => $image_url
                        );
                    }

                    foreach($new_post_image_paths as $image_index => $dest_path_info) {

                        //check for duplicate images, only for existing products
                        if($existing_product !== null && intval($post_data['product_image_skip_duplicates'][$key]) == 1) {
                            $existing_attachment_query = array(
                                'numberposts' => 1,
                                'meta_key' => '_import_source',
                                'post_status' => 'inherit',
                                'post_parent' => $existing_product->ID,
                                'meta_query' => array(
                                    array(
                                        'key'=>'_import_source',
                                        'value'=> $dest_path_info['source'],
                                        'compare' => '='
                                    )
                                ),
                                'post_type' => 'attachment');
                            $existing_attachments = get_posts($existing_attachment_query);
                            if(is_array($existing_attachments) && sizeof($existing_attachments) > 0) {
                                //we've already got this file.
                                $new_post_messages[] = sprintf( __( 'Skipping import of duplicate image %s.', 'pwm-schools-importer' ), $dest_path_info['source'] );
                                continue;
                            }
                        }

                        //make sure we actually got the file.
                        if(!file_exists($dest_path_info['path'])) {
                            $new_post_errors[] = sprintf( __( 'Couldn\'t find local file %s.', 'pwm-schools-importer' ), $dest_path_info['path'] );
                            continue;
                        }

                        $dest_url = str_ireplace(ABSPATH, home_url('/'), $dest_path_info['path']);
                        $path_parts = pathinfo($dest_path_info['path']);

                        //add a post of type 'attachment' so this item shows up in the WP Media Library.
                        //our imported product will be the post's parent.
                        $wp_filetype = wp_check_filetype($dest_path_info['path']);
                        $attachment = array(
                            'guid' => $dest_url,
                            'post_mime_type' => $wp_filetype['type'],
                            'post_title' => preg_replace('/\.[^.]+$/', '', $path_parts['filename']),
                            'post_content' => '',
                            'post_status' => 'inherit'
                        );
                        if ($dest_path_info['type'] == 'book') {
                            $attachment_id = wp_insert_attachment( $attachment, $dest_path_info['path'], $new_post_id );
                        } elseif ($dest_path_info['type'] == 'school') {
                            $attachment_id = wp_insert_attachment( $attachment, $dest_path_info['path'] );
                        }
                        
                        // you must first include the image.php file
                        // for the function wp_generate_attachment_metadata() to work
                        require_once(ABSPATH . 'wp-admin/includes/image.php');
                        $attach_data = wp_generate_attachment_metadata( $attachment_id, $dest_path_info['path'] );
                        wp_update_attachment_metadata( $attachment_id, $attach_data );

                        //keep track of where the attachment came from so we don't import duplicates later
                        add_post_meta($attachment_id, '_import_source', $dest_path_info['source'], true) or
                            update_post_meta($attachment_id, '_import_source', $dest_path_info['source']);

                        //set the image as featured if it is the first image in the set AND
                        //the user checked the box on the preview page.
                        if ($dest_path_info['type'] == 'book') {
                            update_post_meta($new_post_id, '_thumbnail_id', $attachment_id);
                        } elseif ($dest_path_info['type'] == 'school') {
                            update_term_meta($new_post_term, 'thumbnail_id', $attachment_id);
                        }
                        
                        
                    }
                }

            } else {
                $new_post_errors[] = __( 'Skipped import of product without a name', 'pwm-schools-importer' );
            }

            //this is returned back to the results page.
            //any fields that should show up in results should be added to this array.
            $inserted_rows[] = array(
                'row_id' => $row_id+1,
                'post_id' => $new_post_id ? $new_post_id : '',
                'term_id' => $new_post_term ? $new_post_term : '',
                'name' => $new_post['post_title'] ? $new_post['post_title'] : '',
                'price' => $new_post_meta['_price'] ? $new_post_meta['_price'] : '',
                'weight' => $new_post_meta['_weight'] ? $new_post_meta['_weight'] : '',
                'has_errors' => (sizeof($new_post_errors) > 0),
                'errors' => $new_post_errors,
                'has_messages' => (sizeof($new_post_messages) > 0),
                'messages' => $new_post_messages,
                'success' => $new_post_insert_success
            );
        }
    }

    echo json_encode(array(
        'remaining_count' => $rows_remaining,
        'row_count' => $row_count,
        'insert_count' => $insert_count,
        'insert_percent' => $insert_percent,
        'inserted_rows' => $inserted_rows,
        'error_messages' => $error_messages,
        'limit' => $limit,
        'new_offset' => ($limit + $offset)
    ));
?>

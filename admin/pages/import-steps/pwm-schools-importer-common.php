<?php 

    if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly
?>
<style type="text/css">
	.pwm_schools_importer_wrapper form { padding: 20px 0; }

    .pwm_schools_importer_wrapper #advanced_settings { display: none; }

    .pwm_schools_importer_wrapper .import_error_messages {
        margin: 6px 0;
        padding: 0;
    }

    .pwm_schools_importer_wrapper .import_error_messages li {
        margin: 2px 0;
        padding: 4px;
        background-color: #f9dede;
        border: 1px solid #ff8e8e;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }

    .pwm_schools_importer_wrapper #import_status {
        padding: 8px 8px 8px 82px;
        min-height: 66px;
        position: relative;
        margin: 6px 0;
        background-color: #fff5d1;
        border: 1px solid #ffc658;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .pwm_schools_importer_wrapper #import_status.complete {
        background-color: #ecfdbe;
        border: 1px solid #a1dd00;
    }

    .pwm_schools_importer_wrapper #import_status img {
        position: absolute;
        top: 8px;
        left: 8px;
    }

    .pwm_schools_importer_wrapper #import_status strong {
        font-size: 18px;
        line-height: 1.2em;
        padding: 6px 0;
        display: block;
    }

    .pwm_schools_importer_wrapper #import_status #import_in_progress { display: block; }
    .pwm_schools_importer_wrapper #import_status.complete #import_in_progress { display: none; }

    .pwm_schools_importer_wrapper #import_status #import_complete { display: none; }
    .pwm_schools_importer_wrapper #import_status.complete #import_complete { display: block; }

    .pwm_schools_importer_wrapper #import_status td,
    .pwm_schools_importer_wrapper #import_status th {
        text-align: left;
        font-size: 13px;
        line-height: 1em;
        padding: 4px 10px 4px 0;
    }

    .pwm_schools_importer_wrapper table th { vertical-align: top; }

    .pwm_schools_importer_wrapper table.super_wide th,
    .pwm_schools_importer_wrapper table.super_wide td {
        width: 120px;
        min-width: 120px;
    }

    .pwm_schools_importer_wrapper table.super_wide th.narrow,
    .pwm_schools_importer_wrapper table.super_wide td.narrow
    .pwm_schools_importer_wrapper table th.narrow,
    .pwm_schools_importer_wrapper table td.narrow {
        width: 65px;
    }
    .pwm_schools_importer_wrapper table input { margin: 1px 0; }

    .pwm_schools_importer_wrapper table tr.header_row th {
        background-color: #DCEEF8;
        background-image: none;
        vertical-align: middle;
    }

    .pwm_schools_importer_wrapper .map_to_settings {
        margin: 2px 0;
        padding: 2px;
        overflow: hidden;
    }
    .pwm_schools_importer_wrapper .map_to_settings select { width: 98%; }

    .pwm_schools_importer_wrapper .field_settings {
        display: none;
        margin: 2px 0;
        padding: 4px;
        background-color: #e0e0e0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }
    .pwm_schools_importer_wrapper .field_settings h4 {
        margin: 0;
        font-size: 0.9em;
        line-height: 1.2em;
    }
    .pwm_schools_importer_wrapper .field_settings p {
        margin: 4px 0;
        overflow: hidden;
        font-size: .9em;
        line-height: 1.3em;
    }
    .pwm_schools_importer_wrapper .field_settings select { width: 98%; }
    .pwm_schools_importer_wrapper .field_settings input[type="text"] { width: 98%; }

    .pwm_schools_importer_wrapper #inserted_rows tr.error td { background-color: #FFF6D3; }
    .pwm_schools_importer_wrapper #inserted_rows tr.fail td { background-color: #FFA8A8; }

    .pwm_schools_importer_wrapper #inserted_rows .icon {
        display: block;
        width: 16px;
        height: 16px;
        background-position: 0 0;
        background-repeat: no-repeat;
    }
    .pwm_schools_importer_wrapper #inserted_rows tr.success .icon { background-image: url('<?php echo PWM_SHIPMENTS_PLUGIN_ROOT_URL; ?>/admin/assets/img/accept.png'); }
    .pwm_schools_importer_wrapper #inserted_rows tr.error .icon { background-image: url('<?php echo PWM_SHIPMENTS_PLUGIN_ROOT_URL; ?>/admin/assets/img/error.png'); }
    .pwm_schools_importer_wrapper #inserted_rows tr.fail .icon { background-image: url('<?php echo PWM_SHIPMENTS_PLUGIN_ROOT_URL; ?>/admin/assets/img/exclamation.png'); }

    .pwm_schools_importer_wrapper #debug {
        display: none;
        font-family: monospace;
        font-size: 14px;
        line-height: 16px;
        color: #333;
        background-color: #f5f5f5;
        border: 1px solid #efefef;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        padding: 0 10px;
    }

    .pwm_schools_importer_wrapper #credits {
        margin: 20px 0 6px;
    }

    .pwm_schools_importer_wrapper #credits p {
        margin: 2px 0;
    }

    .pwm_schools_importer_wrapper #donate_form {
        float: left;
        margin: 0 6px;
        padding: 0;
    }

    .pwm_schools_importer_wrapper #donate_form form {
        margin: 0;
        padding: 0;
    }


    .woocommerce-progress-form-wrapper {
    text-align: center;
    max-width: 700px;
    margin: 40px auto;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content, .woocommerce-progress-form-wrapper .woocommerce-importer {
    background: #fff;
    overflow: hidden;
    padding: 0;
    margin: 0 0 16px;
    box-shadow: 0 1px 3px rgba(0,0,0,.13);
    color: #555;
    text-align: left;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content header, .woocommerce-progress-form-wrapper .woocommerce-importer header {
    border-bottom: 1px solid #eee;
    margin: 0;
    padding: 24px 24px 0;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content h2, .woocommerce-progress-form-wrapper .woocommerce-importer h2 {
    margin: 0 0 24px;
    color: #555;
    font-size: 24px;
    font-weight: 400;
    line-height: 1em;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content p, .woocommerce-progress-form-wrapper .woocommerce-exporter p, .woocommerce-progress-form-wrapper .woocommerce-importer p {
    font-size: 1em;
    line-height: 1.75em;
    font-size: 16px;
    color: #555;
    margin: 0 0 24px;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table, .woocommerce-progress-form-wrapper .woocommerce-exporter .wc-importer-error-log-table, .woocommerce-progress-form-wrapper .woocommerce-exporter .wc-importer-mapping-table, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-error-log-table, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table {
    margin: 0;
    border: 0;
    box-shadow: none;
    width: 100%;
    table-layout: fixed;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table th:first-child, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table td:first-child {
    padding-left: 24px;
    font-size: 1.1em;
}
.woocommerce-progress-form-wrapper .wc-actions {
    overflow: hidden;
    border-top: 1px solid #eee;
    margin: 0;
    padding: 23px 24px 24px;
    line-height: 3em;
}
.woocommerce-progress-form-wrapper .button {
    font-size: 1.25em;
    padding: .5em 1em!important;
    line-height: 1.5em!important;
    margin-right: .5em;
    margin-bottom: 2px;
    height: auto!important;
    border-radius: 4px;
    background-color: #bb77ae;
    border-color: #a36597;
    box-shadow: inset 0 1px 0 rgba(255,255,255,.25), 0 1px 0 #a36597;
    text-shadow: 0 -1px 1px #a36597, 1px 0 1px #a36597, 0 1px 1px #a36597, -1px 0 1px #a36597;
    margin: 0;
    opacity: 1;
}
.woocommerce-progress-form-wrapper .wc-actions .button {
    float: right;
}
.woocommerce-exporter-wrapper .button:active, .woocommerce-exporter-wrapper .button:focus, .woocommerce-exporter-wrapper .button:hover, .woocommerce-importer-wrapper .button:active, .woocommerce-importer-wrapper .button:focus, .woocommerce-importer-wrapper .button:hover, .woocommerce-progress-form-wrapper .button:active, .woocommerce-progress-form-wrapper .button:focus, .woocommerce-progress-form-wrapper .button:hover {
    background: #a36597;
    border-color: #a36597;
    box-shadow: inset 0 1px 0 rgba(255,255,255,.25), 0 1px 0 #a36597;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table, .woocommerce-progress-form-wrapper .woocommerce-exporter .wc-importer-error-log-table, .woocommerce-progress-form-wrapper .woocommerce-exporter .wc-importer-mapping-table, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-error-log-table, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table {
    margin: 0;
    border: 0;
    box-shadow: none;
    width: 100%;
    table-layout: fixed;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table .wc-importer-mapping-table-name,  .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table .wc-importer-mapping-table-name {
    width: 50%;
}
.woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table td:last-child {
    padding-right: 24px;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table td, .woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table th, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table td {
    border: 0;
    padding: 12px;
    vertical-align: middle;
    word-wrap: break-word;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table th, .woocommerce-progress-form-wrapper .woocommerce-exporter .wc-importer-error-log-table th, .woocommerce-progress-form-wrapper .woocommerce-exporter .wc-importer-mapping-table th, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-error-log-table th, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table th {
    font-weight: 700;
}
.woocommerce-progress-form-wrapper .wc-progress-form-content .wc-importer-mapping-table tbody tr:nth-child(odd) td, .woocommerce-progress-form-wrapper .woocommerce-importer .wc-importer-mapping-table tbody tr:nth-child(odd) td {
    background: #fbfbfb;
}
</style>
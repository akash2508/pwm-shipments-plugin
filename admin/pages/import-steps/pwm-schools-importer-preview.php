<?php 

    if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly

    ini_set("auto_detect_line_endings", true);

    //get separator options
    $import_csv_separator = isset($_POST['import_csv_separator']) && strlen($_POST['import_csv_separator']) == 1 ? $_POST['import_csv_separator'] : ',';

    $error_messages = array();

    if(isset($_FILES['import_csv']['tmp_name'])) {

        if(function_exists('wp_upload_dir')) {
            $upload_dir = wp_upload_dir();
            $upload_dir = $upload_dir['basedir'].'/csv_import';
        } else {
            $upload_dir = dirname(__FILE__).'/uploads';
        }

        if(!file_exists($upload_dir)) {
            $old_umask = umask(0);
            mkdir($upload_dir, 0755, true);
            umask($old_umask);
        }
        if(!file_exists($upload_dir)) {
            $error_messages[] = sprintf( __( 'Could not create upload directory %s.', 'pwm-schools-importer' ), $upload_dir );
        }

        //gets uploaded file extension for security check.
        $uploaded_file_ext = strtolower(pathinfo($_FILES['import_csv']['name'], PATHINFO_EXTENSION));

        //full path to uploaded file. slugifys the file name in case there are weird characters present.
        $uploaded_file_path = $upload_dir.'/'.sanitize_title(basename($_FILES['import_csv']['name'],'.'.$uploaded_file_ext)).'.'.$uploaded_file_ext;

        if($uploaded_file_ext != 'csv') {
            $error_messages[] = sprintf( __( 'The file extension %s is not allowed.', 'pwm-schools-importer' ), $uploaded_file_ext );

        } else {

            if(move_uploaded_file($_FILES['import_csv']['tmp_name'], $uploaded_file_path)) {
                $file_path = $uploaded_file_path;

            } else {
                $error_messages[] = sprintf( __( '%s returned false.', 'pwm-schools-importer' ), '<code>' . move_uploaded_file() . '</code>' );
            }
        }
    }

    if($file_path) {
        //now that we have the file, grab contents
        $import_data = array();

        if ( ($handle = fopen($file_path, "r")) !== FALSE ) {
        	while ( ( $line = fgetcsv($handle, 0, $import_csv_separator) ) !== FALSE ) {
                $import_data[] = $line;
            }
            fclose( $handle );

        } else {
            $error_messages[] = __( 'Could not open file.', 'pwm-schools-importer' );
        }

        if(intval($_POST['header_row']) == 1 && sizeof($import_data) > 0)
            $header_row = array_shift($import_data);

        $row_count = sizeof($import_data);
        if($row_count == 0)
            $error_messages[] = __( 'No data to import.', 'pwm-schools-importer' );

    }

    //'mapping_hints' should be all lower case
    //(a strtolower is performed on header_row when checking)
    $col_mapping_options = array(

        'do_not_import' => array(
            'label' => __( 'Do Not Import', 'pwm-schools-importer' ),
            'mapping_hints' => array()),

        'optgroup_general' => array(
            'optgroup' => true,
            'label' => 'Book'),

        'post_title' => array(
            'label' => __( 'Book Name', 'pwm-schools-importer' ),
            'mapping_hints' => array('book title', 'book name')),
        'post_content' => array(
            'label' => __( 'Book Description', 'pwm-schools-importer' ),
            'mapping_hints' => array('book desc', 'book content')),
        '_regular_price' => array(
            'label' => __( 'Book Price', 'pwm-schools-importer' ),
            'mapping_hints' => array('book price')),
        '_weight' => array(
            'label' => __( 'Book Weight', 'pwm-schools-importer' ),
            'mapping_hints' => array('book weight')),
        'product_image_by_url' => array(
            'label' => __( 'Book Images (By URL)', 'pwm-schools-importer' ),
            'mapping_hints' => array('book image', 'book images')),

        'optgroup_status' => array(
            'optgroup' => true,
            'label' => 'School'),

        'product_cat_by_name' => array(
            'label' => __( 'School Name', 'pwm-schools-importer' ),
            'mapping_hints' => array('school name')),
        'product_cat_description' => array(
            'label' => __( 'School Description', 'pwm-schools-importer' ),
            'mapping_hints' => array('school description')),
        'product_cat_image' => array(
            'label' => __( 'School Image (By URL)', 'pwm-schools-importer' ),
            'mapping_hints' => array('school image')),
    );

?>
<div class="pwm_schools_importer_wrapper wrap">
    <div id="icon-tools" class="icon32"><br /></div>
    <h2><?php _e( 'Import Schools and Books &raquo; Preview', 'pwm-schools-importer' ); ?></h2>

    <?php if(sizeof($error_messages) > 0): ?>
        <ul class="import_error_messages">
            <?php foreach($error_messages as $message):?>
                <li><?php echo $message; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <div class="woocommerce-progress-form-wrapper">
    <?php if($row_count > 0): ?>
        <form class="wc-progress-form-content woocommerce-importer" enctype="multipart/form-data" method="post" action="<?php echo get_admin_url().'admin.php?page=pwm-schools-importer&action=result'; ?>">
            <input type="hidden" name="uploaded_file_path" value="<?php echo htmlspecialchars($file_path); ?>">
            <input type="hidden" name="import_csv_separator" value="<?php echo htmlspecialchars($import_csv_separator); ?>">
            <input type="hidden" name="header_row" value="<?php echo $_POST['header_row']; ?>">
            <input type="hidden" name="row_count" value="<?php echo $row_count; ?>">
            <input type="hidden" name="limit" value="5">

            <header>
                <h2><?php _e( 'Map CSV fields', 'pwm-schools-importer' ); ?></h2>
                <p><?php _e( 'Select fields from your CSV file to map against book and school fields, or to ignore during import.', 'pwm-schools-importer' ); ?></p>
            </header>

            <section class="wc-importer-mapping-table-wrapper">
                <table class="widefat wc-importer-mapping-table" cellspacing="0">
                   <thead>
                        <tr>
                            <th><?php _e( 'Column name', 'pwm-schools-importer' ); ?></th>
                            <th><?php _e( 'Map to field', 'pwm-schools-importer' ); ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php if(intval($_POST['header_row']) == 1): ?>
                            <?php foreach($header_row as $key => $col): ?>
                                <tr>
                                    <td class="wc-importer-mapping-table-name"><?php echo htmlspecialchars($col); ?></td>
                                    <td class="wc-importer-mapping-table-name">
                                        <div class="map_to_settings">
                                            <select name="map_to[<?php echo $key; ?>]" class="map_to">
                                                <optgroup>
                                                <?php foreach($col_mapping_options as $value => $meta): ?>
                                                    <?php if(array_key_exists('optgroup', $meta) && $meta['optgroup'] === true): ?>
                                                        </optgroup>
                                                        <optgroup label="<?php echo $meta['label']; ?>">
                                                    <?php else: ?>
                                                        <option value="<?php echo $value; ?>" <?php
                                                            if(intval($_POST['header_row']) == 1) {
                                                                //pre-select this value if the header_row
                                                                //matches the label, value, or any of the hints.
                                                                $header_value = strtolower($header_row[$key]);
                                                                if( $header_value == strtolower($value) ||
                                                                    $header_value == strtolower($meta['label']) ||
                                                                    in_array($header_value, $meta['mapping_hints']) ) {
                                                                    echo 'selected="selected"';
                                                                }
                                                            }
                                                        ?>><?php echo $meta['label']; ?></option>
                                                    <?php endif;?>
                                                <?php endforeach; ?>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                <!-- <tbody>
                    <?php foreach($import_data as $row_id => $row): ?>
                        <tr>
                            <?php foreach($row as $col): ?>
                                <td><?php echo htmlspecialchars($col); ?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody> -->
                </table>
            </section>
            <div class="wc-actions">
                <button class="button button-primary button-next" value="Run the importer" type="submit"><?php _e( 'Run the importer', 'pwm-schools-importer' ); ?></button>
            </div>
        </form>
    <?php endif; ?>
    </div>
</div>
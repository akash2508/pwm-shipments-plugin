<?php

    if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly
?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $("#show_advanced_settings").click(function(){
            $("#advanced_settings").show(400);
            $(this).hide(400);
        });
    });
</script>

<div class="pwm_schools_importer_wrapper wrap">
    <div id="icon-tools" class="icon32"><br /></div>
    <h2><?php _e( 'Import Schools and Books &raquo; Upload', 'pwm-schools-importer' ); ?></h2>

    <form enctype="multipart/form-data" method="post" action="<?php echo get_admin_url().'admin.php?page=pwm-schools-importer&action=preview'; ?>">
        <table class="form-table">
            <tbody>
                <tr>
                    <th>
                        <label for="import_csv"><?php _e( 'File to Import (CSV)', 'pwm-schools-importer' ); ?></label><br/>
                        <span><a href="<?php echo PWM_SHIPMENTS_PLUGIN_ROOT_URL; ?>/admin/assets/sample-csv/schools-books-import.csv">Sample CSV</a></span>
                    </th>
                    <td><input type="file" name="import_csv" id="import_csv"></td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <button class="button-primary" type="submit"><?php _e( 'Upload and Preview', 'pwm-schools-importer' ); ?></button>
                        <button class="button" type="button" id="show_advanced_settings"><?php _e( 'Advanced Settings &darr;', 'pwm-schools-importer' ); ?></button>
                    </td>
                </tr>
            </tbody>
        </table>

        <div id="advanced_settings">
            <h3><?php _e( 'Advanced Settings', 'pwm-schools-importer' ); ?></h3>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th><label for="header_row"><?php _e( 'First Row is Header Row', 'pwm-schools-importer' ); ?></label></th>
                        <td>
                            <input type="checkbox" name="header_row" id="header_row" value="1" checked="checked">
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e( 'Path to Your <strong>uploads</strong> Folder', 'pwm-schools-importer' ); ?></th>
                        <td><?php
                            $upload_dir = wp_upload_dir();
                            echo $upload_dir['basedir'];
                        ?></td>
                    </tr>
                    <tr>
                        <th><?php _e( 'CSV field separator', 'pwm-schools-importer' ); ?></th>
                        <td>
                            <input type="text" name="import_csv_separator" id="import_csv_separator" class="code" value="," maxlength="1">
                            <p class="description"><?php _e( 'Enter the character used to separate each field in your CSV. The default is the comma (,) character. Some formats use a semicolon (;) instead.', 'pwm-schools-importer' ); ?></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>

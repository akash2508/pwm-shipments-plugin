<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/admin
 * @author     PWM
 */
class PWM_Shipments_Admin extends PWM_Shipments_Admin_Pages
{
    private $plugin_name;
    
    private $version;

    private $parent;

    /**
    * Initialize the class and set its properties.
    *
    * @since    1.0.0
    * @param      string    $plugin_name       The name of this plugin.
    * @param      string    $version    The version of this plugin.
    */
    public function __construct($plugin_name, $version) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->parent = new PWM_Shipments_Admin_Pages();
    }


    public function pwm_shipments_pages()
    {
        add_menu_page(
                'PWM Shipments', 
                'All Shipments', 
                'manage_options', 
                'pwm-shipments', 
                array( $this->parent,'pwm_shipments_admin_dashboard'), 
                PWM_SHIPMENTS_PLUGIN_ROOT_URL.'/admin/assets/dash-icons/shipping.png',
                58
        );

        add_submenu_page(
                'pwm-shipments', 
                'Akademos PO Files', 
                "Akademos PO Files", 
                'manage_options', 
                'pwm-shipments-akademos-pos', 
                array( $this->parent,'pwm_shipments_admin_akademos_pos')
        );

        add_submenu_page(
                'pwm-shipments', 
                'Process Akademos NOW', 
                "Process Akademos NOW", 
                'manage_options', 
                'pwm-shipments-akademos-process-now', 
                array( $this->parent,'pwm_shipments_admin_akademos_process_now')
        );

        add_submenu_page(
                'pwm-shipments',
                'Manually Handle Errors',
                'Manually Handle Errors',
                'manage_options',
                'pwm-shipments-manual-handle-errors',
                array( $this->parent, 'pwm_shipments_manual_handle_errors')
        );

        add_submenu_page(
                'pwm-shipments', 
                'Packing List', 
                "Packing List", 
                'manage_options', 
                'pwm-shipments-akademos-packing-list', 
                array( $this, 'pwm_shipments_admin_akademos_packing_list')
        );

        add_submenu_page(
                'pwm-shipments', 
                'Inventory', 
                "Inventory", 
                'manage_options', 
                'pwm-shipments-inventory', 
                array( $this->parent,'pwm_shipments_admin_inventory' )
        );

        add_submenu_page(
                'pwm-shipments', 
                'Monthly Report', 
                "Reports", 
                'manage_options', 
                'pwm-shipments-monthly-report', 
                array( $this->parent, 'pwm_shipments_admin_monthly_report')
        );

        // add_submenu_page(
        //         'pwm-shipments', 
        //         'Import Schools',
        //         'Import Schools',
        //         'manage_options', 
        //         'pwm-schools-importer', 
        //         array( $this->parent, 'pwm_import_schools_books')
        // );

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function pwm_shipments_enqueue_scripts() {
        /**
         * This function is provided for including external assets/js file
         */
        wp_enqueue_script( $this->plugin_name, plugin_dir_url(__FILE__) . '/assets/js/pwm-shipments.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'pwm_ajax', array( 'upload_url' => admin_url('async-upload.php'), 'ajaxurl'   => admin_url('admin-ajax.php'), 'nonce'      => wp_create_nonce('media-form')));

        wp_enqueue_style( $this->plugin_name, plugin_dir_url(__FILE__) . 'assets/css/pwm-shipments.css');

    }
    
}
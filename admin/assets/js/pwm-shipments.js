jQuery(document).ready(function(){
	jQuery('.confirm-click').on('click', function () {
		return confirm('Are you sure you want to delete this?');
	});
});

//Inventory import function
(function($) {
    $(document).ready(function() {
        var $import_form    = $('#import_csv');
        var $importMessage  = $import_form.find('.import-message');
        console.log($importMessage);
        var $csvFile        = $import_form.find('#import_inventory');
        var $csvId          = $import_form.find('[name="csv_id"]');

        if ( $import_form.length ) {
            $csvFile.on('click', function() {
                $(this).val('');
                $csvId.val('');
            });

            $import_form.on( 'click', '.btn-change-image', function(e) {
                e.preventDefault();
                $csvFile.val('').show();
                $csvId.val('');
            });
            
            //Upload .csv file
            $csvFile.on('change', function(e) {
                e.preventDefault();

                var formData = new FormData();
                
                formData.append('action', 'pwm_action_inventory_import');
                formData.append('async-upload', $csvFile[0].files[0]);
                formData.append('name', $csvFile[0].files[0].name);
                formData.append('_wpnonce', pwm_ajax.nonce);
                
                $.ajax({
                    url: pwm_ajax.upload_url,
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();

                        if ( myXhr.upload ) {
                            myXhr.upload.addEventListener( 'progress', function(e) {
                                if ( e.lengthComputable ) {
                                    var perc = ( e.loaded / e.total ) * 100;
                                    perc = perc.toFixed(2);
                                    $importMessage.html('Uploading&hellip;(' + perc + '%)');
                                }
                            }, false );
                        }

                        return myXhr;
                    },
                    type: 'POST',
                    beforeSend: function() {
                        $csvFile.hide();
                        $importMessage.html('Uploading&hellip;').show();
                    },
                    success: function(resp) {
                        $csvId.val( resp );
                        $csvFile.show();
                        $importMessage.append(' Successfully uploaded. <a href="#" class="btn-change-image">Change?</a>');
                        $importMessage.show();
                    },
                    error: function(resp) {
                        $importMessage.empty();
                        $importMessage.append(resp.responseText);
                        $importMessage.show();
                        $csvFile.show();
                    }
                });
            });

            //Process and Import .csv file data
            $import_form.on('submit', function(e) {
                e.preventDefault();

                var data = $(this).serialize();

                $.post( pwm_ajax.ajaxurl, data, function(resp) {
                    console.log(resp);
                    if ( resp.success ) {
                        $import_form[0].reset();
                        $importMessage.empty();
                        $csvFile.val('').show();
                        $csvId.val('');
                        $importMessage.append('Inventory successfully imported.');
                        $importMessage.show();
                        location.reload(true);
                    } else {
                        $importMessage.empty();
                        $importMessage.append('Failed to import file data. Please check your file.');
                        $importMessage.show();
                    }
                });
            });
        }
        
        var $single_entry_form    = $('#add_inventory');
        var $insertMessage  = $single_entry_form.find('.insert-message');
        var $order_id        = $single_entry_form.find('#order_id');
        var $barcode        = $single_entry_form.find('#barcode');
        var $price        = $single_entry_form.find('#price');
        var $weight        = $single_entry_form.find('#weight');
        
        var seformData = new FormData();
                
        seformData.append('action', 'pwm_action_add_single_inventory');
        seformData.append('_wpnonce', pwm_ajax.nonce);
        //Process and Import .csv file data
        $single_entry_form.on('submit', function(e) {
            e.preventDefault();

            var data = $(this).serialize();

            $.post( pwm_ajax.ajaxurl, data, function(resp) {
                if ( resp.success ) {
                    $insertMessage.empty();
                    $insertMessage.append(resp.data.msg);
                    $insertMessage.show();
                } else {
                    $insertMessage.empty();
                    $insertMessage.append(resp.data.msg);
                    $insertMessage.show();
                }
            });
        });
        /*$(document).ajaxStop(function(){
            window.location.reload();
        });*/
    });
})(jQuery);
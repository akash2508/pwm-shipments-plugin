<?php
/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during the plugin's activation.
 * @package    pwm-shipments
 * @subpackage pwm-shipments/includes
 * @author     PWM
 */

class PWM_Shipments_Deactivator
{
	public static function deactivate()
	{
		/*$timestamp = wp_next_scheduled( 'was_cronjob' );
		wp_unschedule_event( $timestamp, 'was_cronjob' );*/
	}

}

<?php
/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    pwm-shipments
 * @subpackage pwm-shipments/includes
 * @author     PWM
 */

class PWM_Main_Shipments

{

    protected $loader;

    protected $plugin_name;

    protected $version;

    protected $errors;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */

    public function __construct() {

        $this->plugin_name = 'pwm-shipments';

        $this->version = '1.0.0';

        $this->load_dependencies();

        $this->define_admin_hooks();

       // $this->define_public_hooks();

    }
    
    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - PWM_Shipments_Loader. Orchestrates the hooks of the plugin.
     * - PWM_Shipments_Admin. Defines all hooks for the admin area.
     * - PWM_Shipments_Actions. Defines all hooks for the actions (download/delete/submit forms etc)
     * - PWM_Shipments_Admin_pages. Defines all hooks for the pages under the admin area.
     * - PWM_Shipments_Shipments_List_Table Defines all hooks for displaying all the shipments in a table layout
     * - PWM_Shipments_Akademos_POFiles Defines all hooks for displaying all the PO akademos PO files in a table layout
     * - PWM_Shipments_Akademos_POFiles Defines all hooks for displaying monthly shipments records.
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */

    private function load_dependencies() {

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pwm-shipments-loader.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/config/pwm-constants.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/cryption.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/zpl.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/woo_order_actions.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/woo_shipment_actions.php';
  
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/general-functions.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/shippers/class-pwm-ups.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/shippers/class-pwm-usps.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/pwmcronhelper.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'library/connections/external/PWMFtpSiteConnector.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/pages/class-pwm-shipments-admin-pages.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/pages/actions/class-pwm-shipments-actions.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-pwm-shipments-admin.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-list-table.php';
        
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-order-errors-table.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-akademos-POfiles.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-admin-akademos-process-now.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-admin-akademos-packing-list.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-admin-monthly-report.php';
		
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-shipments-inventory.php';
        
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-pwm-schools-importer.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'general/cron.php';

        $this->loader = new PMW_Shipments_Loader();

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     * - PWM_Shipments_Monthly_Reports action is defined to submit the search form and generate/download a report.pdf file.
     * - PWM_Shipments_Monthly_Reports action is defined to submit the search form and generate/download a report.pdf file.
     *
     * @since    1.0.0
     * @access   private
     */
    
    private function define_admin_hooks() {
        $plugin_admin = new PWM_Shipments_Admin( $this->get_plugin_name(), $this->get_version() );
        $plugin_actions = new PWM_Shipments_Actions( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'pwm_shipments_pages', 99 );
        $this->loader->add_action( 'admin_post_pwm_shipments_monthly_report', $plugin_actions, 'pwm_shipments_admin_monthly_report_download');
        $this->loader->add_action( 'admin_init', $plugin_actions, 'pwm_shipments_download_packinglist_action');
        $this->loader->add_action( 'wp_ajax_pwm_action_inventory_import', $plugin_actions, "pwm_action_inventory_import");
        $this->loader->add_action( 'wp_ajax_nopriv_pwm_action_inventory_import', $plugin_actions, "pwm_action_inventory_import");
        $this->loader->add_action( 'wp_ajax_pwm_schools_importer_ajax', $plugin_actions, "pwm_schools_importer_ajax");
        $this->loader->add_action( 'wp_ajax_pwm_shipments_save_error_table_data', $plugin_actions, "pwm_shipments_save_error_table_data");
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'pwm_shipments_enqueue_scripts' );
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */

    public function run() {

        $this->loader->run();

    }



    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */

    public function get_plugin_name() {

        return $this->plugin_name;

    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    WooCommerce_Advanced_Shipping_Loader    Orchestrates the hooks of the plugin.
     */

    public function get_loader() {

        return $this->loader;

    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */

    public function get_version() {

        return $this->version;

    }

}
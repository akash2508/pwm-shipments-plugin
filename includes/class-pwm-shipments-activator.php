<?php
/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during the plugin's activation.
 * @package    pwm-shipments
 * @subpackage pwm-shipments/includes
 * @author     PWM
 */
class PWM_Shipments_Activator
{

    public static function activate()
    {
		global $wpdb;
        /*$main_table = $wpdb->prefix . "was_advanced_shipping";
		$option_table = $wpdb->prefix . "was_options";
        $option_choices_table = $wpdb->prefix . "was_option_choices";
        $option_conditions_table = $wpdb->prefix . "was_option_conditions";
        $option_variation_table = $wpdb->prefix. 'was_option_variation';
		$sql = array();

		if ( ! empty($wpdb->charset) ) {
            $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		}

        if ( ! empty($wpdb->collate) ) {
            $charset_collate .= " COLLATE $wpdb->collate";
		}

        $sql[] = "CREATE TABLE $main_table (
            id mediumint(8) unsigned not null auto_increment,
            product_id mediumint(8) unsigned not null,
            option_id mediumint(8) unsigned not null,
            meta longtext,
            PRIMARY KEY (id),
            UNIQUE KEY `product_id` (`product_id`,`option_id`)
        )$charset_collate;";

		$sql[] = "CREATE TABLE $option_table (
			id mediumint(8) unsigned not null auto_increment,
			name text,
            description text,
			meta longtext,
			PRIMARY KEY (id)
		)$charset_collate;";

        $sql[] = "CREATE TABLE $option_choices_table (
			id mediumint(8) unsigned not null auto_increment,
			name text,
			PRIMARY KEY (id)
        )$charset_collate;";

         $sql[] = "CREATE TABLE $option_conditions_table (
			id mediumint(8) unsigned not null auto_increment,
			name text,
			PRIMARY KEY (id)
		)$charset_collate;";

        $sql[] = "CREATE TABLE $option_variation_table (
			id mediumint(8) unsigned not null auto_increment,
			product_option_id mediumint(8) unsigned not null,
            variation_id mediumint(8) unsigned not null,
			PRIMARY KEY (id)
        )$charset_collate;";

        require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option( 'was_db_version', '1.1' );*/

    }

    public static function preload_data()
    {
        /*global $wpdb;

        $option_choices_table = $wpdb->prefix . "was_option_choices";
        $choices = array(
            'Flat rate shipping',
            'Free shipping',
            'Carrier',
            'Packages per item',
            'Manual shipping',
            'Flat or Manual'
        );

        $choice_cnt = $wpdb->get_var( "SELECT COUNT(*) FROM $option_choices_table" );

        if ($choice_cnt == 0) {
            foreach( $choices as $choice ) {

                error_log($choice);
                $wpdb->insert(
                    $option_choices_table,
                    array( 'name' => $choice )
                );
            }
        }

        $option_conditions_table = $wpdb->prefix . "was_option_conditions";
        $conditions = array(
            'All',
            'Amount',
            'Only',
            '=',
            '>=',
            '>',
            '<=',
            '<',
            '+',
            '*',
            'Between'
        );

        $condition_cnt = $wpdb->get_var( "SELECT COUNT(*)FROM $option_conditions_table" );
        if( $condition_cnt == 0) {
            foreach( $conditions as $condition ) {
                $wpdb->insert(
                    $option_conditions_table,
                    array( 'name' => $condition )
                );
            }
        }*/

    }
}

<?php
/**
 * The cron scheduled actions.
 *
 *
 * @since      1.0.0
 * @package    pwm-shipments
 * @subpackage pwm-shipments/general
 * @author     PWM
 */

if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly

/**
 * Custom cron schedules recurrence
 */
add_filter( 'cron_schedules', 'pwm_cron_job_recurrence' );
function pwm_cron_job_recurrence( $schedules )
{
    $schedules['every_15_minutes'] = array(
        'interval' => 15 * MINUTE_IN_SECONDS,
        'display'  => __( 'Every 15 Minutes' ),
    );
    return $schedules;
}

/** 
 * cron for save api info data
 */
function pwmDBCronSaveApiInfo()
{
    $dbDataJson = json_encode(array(
        'DB_NAME' => DB_NAME,
        'DB_USER' => DB_USER,
        'DB_PASSWORD' => DB_PASSWORD,
        'DB_HOST' => DB_HOST,
    ));

    $cryption = new PWM_Cryption();
    $encryptedData = ($cryption->pwm_general_encrypt("pwmWpDbCron", $dbDataJson));
    file_put_contents(PWM_SHIPMENTS_PLUGIN_ROOT_DIR.'data/api.info', $encryptedData);
}
add_action( 'pwm_db_cron_save_api_info', 'pwmDBCronSaveApiInfo' );

/**
 * Run cron process for PO files on Akademos.
 */
function pwmShipmentsAkademosCronDailyDispatcher()
{    
	$process_now = new pwm_shipments_admin_akademos_process_now();
    $process_now->akademosProcessPOs();
}
add_action( 'pwm_shipments_akademos_cron_daily_dispatcher', 'pwmShipmentsAkademosCronDailyDispatcher' );
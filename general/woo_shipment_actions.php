<?php

add_filter('woocommerce_package_rates', 'pwm_shipments_shipping_woo_shipments_mod_rates');
function pwm_shipments_shipping_woo_shipments_mod_rates($rates) {
    foreach($rates as $rate ) {
        if ($rate->method_id === 'wf_shipping_ups') {
            $rate->cost += 3.50;
        }
    }
    return $rates;
}

function pwm_shipments_shipping_woo_shipments_set_default_order( $orderId ){
    if ( !$orderId ) return;
    $order = new WC_Order( $orderId );
    $order->update_status( 'completed' );
}
add_action( 'woocommerce_thankyou', 'pwm_shipments_shipping_woo_shipments_set_default_order' );
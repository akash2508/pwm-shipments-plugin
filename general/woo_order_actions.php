<?php

add_action( 'woocommerce_order_actions', 'pwmShipmentsAkademosWooAddOrderMetaBoxActions' );
function pwmShipmentsAkademosWooAddOrderMetaBoxActions($actions) {
    if( !is_array($actions) ) {
        $actions = array();
    }
    $actions[PWM_SHIPMENTS_AKADEMOS_WOO_ORDER_ACTION] = "Send order to Akademos";
    return $actions;
}

/**
 * hook when save action is pressed
 */
add_action( 'woocommerce_order_action_'.PWM_SHIPMENTS_AKADEMOS_WOO_ORDER_ACTION, 'pwmShipmentsAkademosWooOrderSendToAkademos' );
function pwmShipmentsAkademosWooOrderSendToAkademos($order) {
    pwmShipmentsAkademosWooSaveOrder($order->id);
    die;
}


/**
 * called when order is marked complete
 */
add_action( 'woocommerce_order_status_completed', 'pwmShipmentsAkademosWooSaveOrder');

/**
 * The fields are as follows: 
 * ----- OrderNumber|PONumber|Barcode|AkademosPrice|Weight (weight is for just one copy of the book).
 * Therefore if someone orders two copies in one order (which has happened) the 
 * Weight would be multiplied by the order quantity. 
 * @param $orderNumber
 * @param $poNumber
 * @param $barcode
 * @param $akademosPrice
 * @param $weight -> QUANTITY
 */
function pwmShipmentsAkademosWooSaveOrder($orderId) {

    $connector = new PWMFtpSiteConnector(PWM_SHIPMENTS_AKADEMOS_FTP_HOST, PWM_SHIPMENTS_AKADEMOS_FTP_USER, PWM_SHIPMENTS_AKADEMOS_FTP_PASS, PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR);

    $products = array();

    $order = new WC_Order($orderId);

    $shippingMethod = $order->get_shipping_method();

    $items = $order->get_items();

    $date = new DateTime();

    $dateFormatStr = $date->format("Ymd");

    $shippingAddress = $order->get_address("shipping");

    foreach ($items as $item) {
        $qty = $item;
        $product = $order->get_product_from_item($item);
        $products[] = array(
            'obj' => $product,
            'qty' => $qty,
        );
        $sku = $product->get_sku();
        $price = $product->get_price();

        //-------------------------------inventory-------------------------------
        $inventoryFile = sprintf("/testing/inventory%s.txt", $dateFormatStr);
        $inventoryLine = sprintf("%s|%s|%.2f\n", $sku, $id, $price);
        $connector->write($inventoryFile, $inventoryLine);

        //-------------------------------shipping--------------------------------
        $shippingFile = sprintf("/testing/shipping/shipping%s.po", $dateFormatStr);
        $shippingLine = sprintf("%d|%d|%d|%d|%s|%s|%s|%s|%s|%s|%s\n", 
            -1, $id, -1, 1, 
            $shippingAddress['first_name'] . ' ' . $shippingAddress['last_name'], 
            $shippingAddress['city'], "", $shippingAddress['state'],
            $shippingAddress['postcode'], $shippingMethod, $trackingNum
        );
        $connector->write($shippingFile, $shippingLine);
    }
}
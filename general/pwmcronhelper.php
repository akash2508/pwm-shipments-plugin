<?php
/**
 * The UPS Shipping functionality of the plugin.
 *
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/general
 * @author     PWM
 */

class PWM_ShippingAkademosCronHelper
{
	protected $connection;

    protected $scanDir, $saveDir;

    public $filesToProcess, $processedFilesCount, $processedOrders;

    public function __construct()
    {
    	$this->connection = new PWMFtpSiteConnector(PWM_SHIPMENTS_AKADEMOS_FTP_HOST, PWM_SHIPMENTS_AKADEMOS_FTP_USER, PWM_SHIPMENTS_AKADEMOS_FTP_PASS, PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR);
        
		$this->scanDir = 'orders/';
		$this->saveDir = "shipping/";

		$this->processedOrders = get_option("pwm-static-akademos-processed-orders", array());
		$this->filesToProcess = array();
		$this->processedFilesCount = 0;
    }

    public function setFilesToProcess()
    {
        $files = $this->connection->scandir($this->scanDir);
        if( is_array($files) ) {
            foreach ($files as $filePath) {
                $file = basename($filePath);
                if (!in_array($file, $this->processedOrders)) {
                    $this->filesToProcess[] = $filePath;
                }
            }
        } else {
            ob_start();
            var_dump($files);
            $msg = "Error scanning $this->scanDir got " . ob_get_clean() . "<br>";
            echo $msg;
        }
    }

    public function processSingleFile($file, $local = false)
    {
    	global $wpdb;

        $this->processedFilesCount++;

        $orders = array();

        if ( $local ) {
            $temp = fopen($file, "r");
        } else {
            $temp = tmpfile();
            $metaDatas = stream_get_meta_data($temp);
            $tmpFilename = $metaDatas['uri'];
            $this->connection->getFile($tmpFilename, $file);
            $fileContents = file_get_contents($tmpFilename);
        }
        $rawLines = explode(PHP_EOL, $fileContents);
        
        echo("Processing $file<br>");
        while (($data = fgetcsv($temp, 0, "|")) !== FALSE && $data !== null) {
            if (count($data) < 11) {
                continue;
            }
            
            $rawInputLine = implode("|", $data);

            $query = "SELECT * FROM {$wpdb->prefix}pwm_shipping_akademos_orders WHERE `input_line` LIKE '".addslashes($rawInputLine)."' AND response != ''";
            $results = $wpdb->get_results($query, ARRAY_A);

            if( count($results) > 0 ) {
                continue;
            }
            
            $inputWithoutTracking = implode("|", array_slice($data, 0, 11));
            $qty = intval($data[3]);
            $bookCode = intval($data[2]);
            $weight = $this->getWeight($bookCode) * $qty;
            $name = $data[4];
            $address1 = $data[5];
            $address2 = $data[6];
            $city = $data[7];
            $state = $data[8];
            $zip = $data[9];
            $shippingMethod = trim($data[10]);

            //generate unique key for each book address
            $shipTo = "$name $address1 $address2 $city $state $zip $shippingMethod";
            if( ! isset($orders[$shipTo]) ) $orders[$shipTo] = array();
            
            $orderData = array(
                'createLabel' => null, //to override in following if
                'data' => array(
                    'qty' => $qty,
                    'bookCode' => $bookCode,
                    'weight' => $weight,
                    'name' => $name,
                    'address1' => $address1,
                    'address2' => $address2,
                    'city' => $city,
                    'state' => $state,
                    'zip' => $zip,
                    'shippingMethod' => $shippingMethod,
                ),
                'inputLineFull' => $rawInputLine,
                'inputLineNoTracking' => $inputWithoutTracking,
            );
            
            if (count($data) === 11) {
                $orderData['createLabel'] = true;
                $orders[$shipTo][] = $orderData;
            } else {
                $orderData['createLabel'] = false;
                $orders[$shipTo][] = $orderData;
            }
        }
        return $orders;
    }

    public function saveFileAfterProcessing($outputFileName, $content)
    {
        //TODO if file exists append contents
        if( ! $this->connection->fileExists($outputFileName) ) {
            $this->connection->write($outputFileName, $content, false);
        } else {
            $this->connection->write($outputFileName, $content, true);
        }
    }

    public function addPoFileToDatabase($isErrored, $poName, $folderPath, $fileName)
    {
    	global $wpdb;
    	
        $insertData = array(
            'status' => $isErrored ? 2 : 1,
            'name' => $fileName,
            'path' => $folderPath,
            'po_name' => $poName,
        );
        $wpdb->insert( "{$wpdb->prefix}pwm_akademos_po", $insertData, 
	        array('%d', '%s', '%s', '%s') 
	    );
    }
    
    public function getWeight($bookCode)
    {
        $inventoryArr = get_option('pwm-shipping-akademos-inventory-arr', array());
        if( ! is_array($inventoryArr) ) $inventoryArr = array();
        
        $weight = -1;
        foreach( $inventoryArr as $row ) {
            $rowBookCode = $row[1];
            if( $rowBookCode === $bookCode ) {
                $rowWeight = $row[3];
                $weight = $rowWeight;
                break;
            }
        }
        return $weight;
    }

    public function updateProcessedOrders($fileName)
    {
        $this->processedOrders[] = $fileName;
        update_option('pwm-static-akademos-processed-orders', $this->processedOrders);
    }

    public function emailDebugInfo($file, $content)
    {
        $from = null;
        $to ="patrick@coursepacksetc.com";
        $subject = "Akademos PO Error - " . basename($file);
        pwmShipmentsHtmlEmail($from, $to, $subject, $content);
        printf("<h1>Error processing %s sent email to %s</h1>", basename($file), $to);
    }

    public function savePo($hasFileErrored, $file, $fileName, $contents)
    {
        //save PO file
        if ($hasFileErrored) {
            $folderPath = $this->saveDir . "error/";
            $this->emailDebugInfo($file, $contents);
        } else {
            //$folderPath = $saveDir . "done/";
            $folderPath = $this->saveDir . "temp/";
        }
        $outputFileName = $folderPath . 'shipping' . date("Ymd") . '.po';
        echo "\n<p><b>$fileName</b> Contents:</p><pre style='margin-left:10px;margin-right:10px;'>$contents</pre>"
        . "\n<p>Saving <b>$fileName</b> at <b>$outputFileName</b></p>";
        echo("Saving PO File to $outputFileName<br>");
        
        $this->saveFileAfterProcessing($outputFileName, $contents);

        $msg = "Moving PO file to $outputFileName";
        echo($msg);
        
        //update database
        $this->addPoFileToDatabase($hasFileErrored, $file, $folderPath, $fileName);
        $this->updateProcessedOrders($fileName);
        pwmShipmentsHtmlEmail('info@CoursePacksEtc.com', 'patrick@coursepacksetc.com', $fileName, $msg, $headers = "");
    }
}
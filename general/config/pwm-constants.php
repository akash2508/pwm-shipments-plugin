<?php
define("PWM_SHIPMENTS_AKADEMOS_FTP_HOST", "hostname");
define("PWM_SHIPMENTS_AKADEMOS_FTP_USER", "username");
define("PWM_SHIPMENTS_AKADEMOS_FTP_PASS", "password");
define("PWM_SHIPMENTS_AKADEMOS_FTP_BASE_DIR", "/");

define("PWM_SHIPMENTS_AKADEMOS_WOO_ORDER_ACTION", "pwm-order-submit-action");

define("PWM_SHIPMENTS_UPS_API_ACCESS_KEY", "upsaccesskey");
define("PWM_SHIPMENTS_UPS_API_USER_ID", "upsuserid");
define("PWM_SHIPMENTS_UPS_API_PASS", "upspassword");
define("PWM_SHIPMENTS_UPS_API_ACCOUNT_NUM", "upsaccountnumber");
define("PWM_SHIPMENTS_UPS_API_BILLING_THIRD_PARTY_ACCOUNT_NUM", "upsthirdpartyaccountnumber");

define("PWM_SHIPMENTS_STAMPS_COM_API_INTEGRATION_ID", "stampscomintegrationid");
define("PWM_SHIPMENTS_STAMPS_COM_API_TEST_USER", "stampscomtestuser");
define("PWM_SHIPMENTS_STAMPS_COM_API_TEST_PASS", "stampscomtestpassword");
define("PWM_SHIPMENTS_STAMPS_COM_API_USER", "stampscomliveuser");
define("PWM_SHIPMENTS_STAMPS_COM_API_PASS", "stampscomlivepassword");

define("PWM_SHIPMENTS_USPS_API_USER_ID", "uspsuserid");
define("PWM_SHIPMENTS_USPS_API_PASS", "uspspassword");


define("PWM_SHIPMENTS_PLUGIN_TMP_DIR", PWM_SHIPMENTS_PLUGIN_ROOT_DIR . 'tmp/');
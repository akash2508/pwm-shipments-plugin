<?php
/**
 * Config file for the AWSP Shipping package.
 * 
 * @package Awsp Shipping Package
 * @author Alex Fraundorf - AlexFraundorf.com
 * @copyright (c) 2012-2013, Alex Fraundorf and AffordableWebSitePublishing.com LLC
 * @version 04/19/2013 - NOTICE: This is beta software.  Although it has been tested, there may be bugs and 
 *      there is plenty of room for improvement.  Use at your own risk.
 * @since 12/02/2012
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 * 
 */
namespace Awsp\Ship;

// absolute path to the directory that contains the Awsp_Ship directory (do not use ending slash)
// example: '/home/usr/libs';
define('AWSP_SHIP_PATH', PWM_SHIPMENTS_PLUGIN_ROOT_DIR. 'vendor/awsp/ship/libs' );

global $pwm_awsp_config;

// configuration options for all shippers
$pwm_awsp_config = array();
// true for production or false for development
$pwm_awsp_config['production_status'] = true; 
// can be 'LB' for pounds or 'KG' for kilograms
$pwm_awsp_config['weight_unit'] = 'LB'; 
// can be 'IN' for inches or 'CM' for centimeters
$pwm_awsp_config['dimension_unit'] = 'IN'; 
// USD for US dollars
$pwm_awsp_config['currency_code'] = 'USD'; 
// if true and if a receiver email address is set, the tracking number will be emailed to the receiver by the 
// shipping vendor
$pwm_awsp_config['email_tracking_number_to_receiver'] = true; 
    

// shipper information
$pwm_awsp_config['shipper_name'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_NAME; 
$pwm_awsp_config['shipper_attention_name'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_ATTENTION_NAME; 
$pwm_awsp_config['shipper_phone'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_PHONE; 
$pwm_awsp_config['shipper_phone_extension'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_PHONE_EXTENSION;  
$pwm_awsp_config['shipper_email'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_EMAIL;
$pwm_awsp_config['shipper_address1'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_ADDRESS_1; 
$pwm_awsp_config['shipper_address2'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_ADDRESS_2;
$pwm_awsp_config['shipper_address3'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_ADDRESS_3; 
$pwm_awsp_config['shipper_city'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_CITY;
$pwm_awsp_config['shipper_state'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_STATE; 
$pwm_awsp_config['shipper_postal_code'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_POSTAL_CODE; 
$pwm_awsp_config['shipper_country_code'] = PWM_SHIPMENTS_AKADEMOS_SHIPPER_COUNTRY_CODE; 

//----------------------------------------------------------------------------------------------------------------------

// UPS shipper configuration settings
// sign up for credentials at: https://www.ups.com/upsdeveloperkit - Note: Chrome browser does not work for this page.
$pwm_awsp_config['ups'] = array();
$pwm_awsp_config['ups']['key'] = PWM_SHIPMENTS_UPS_API_ACCESS_KEY;
$pwm_awsp_config['ups']['user'] = PWM_SHIPMENTS_UPS_API_USER_ID;
$pwm_awsp_config['ups']['password'] = PWM_SHIPMENTS_UPS_API_PASS;
$pwm_awsp_config['ups']['account_number'] = PWM_SHIPMENTS_UPS_API_ACCOUNT_NUM;
$pwm_awsp_config['ups']['testing_url'] = 'https://wwwcie.ups.com/webservices';
$pwm_awsp_config['ups']['production_url'] = 'https://onlinetools.ups.com/webservices'; 
// absolute path to the UPS API files relateive to the Ups.php file
$pwm_awsp_config['ups']['path_to_api_files'] = AWSP_SHIP_PATH . '/Awsp/Ship/ups_api_files'; 

// shipper information - make any necessary overrides
// note: needs to match information on file with UPS or the API call will fail
$pwm_awsp_config['ups']['shipper_name'] = $pwm_awsp_config['shipper_name']; 
$pwm_awsp_config['ups']['shipper_attention_name'] = $pwm_awsp_config['shipper_attention_name']; 
$pwm_awsp_config['ups']['shipper_phone'] = $pwm_awsp_config['shipper_phone']; 
$pwm_awsp_config['ups']['shipper_email'] = $pwm_awsp_config['shipper_email'];
$pwm_awsp_config['ups']['shipper_address1'] = $pwm_awsp_config['shipper_address1']; 
$pwm_awsp_config['ups']['shipper_address2'] = $pwm_awsp_config['shipper_address2'];
$pwm_awsp_config['ups']['shipper_address3'] = $pwm_awsp_config['shipper_address3']; 
$pwm_awsp_config['ups']['shipper_city'] = $pwm_awsp_config['shipper_city'];
$pwm_awsp_config['ups']['shipper_state'] = $pwm_awsp_config['shipper_state']; 
$pwm_awsp_config['ups']['shipper_postal_code'] = $pwm_awsp_config['shipper_postal_code']; 
$pwm_awsp_config['ups']['shipper_country_code'] = $pwm_awsp_config['shipper_country_code']; 

//Define 3rd party shipping for akademos so coursepacks doesn't pay for shipping
$pwm_awsp_config['ups']['PaymentInformation'] = array(
    'ShipmentCharge' => array(
        'BillThirdParty' => array(
//            'BillThirdPartyShipper' => array(
                'AccountNumber' => PWM_SHIPMENTS_UPS_API_BILLING_THIRD_PARTY_ACCOUNT_NUM,
//                'ThirdParty' => array(
                    'Address' => array(
                        'PostalCode' => '06854',
                        'CountryCode' => 'US',
                    ),
//                ),
//            ),
        ), 
    ), 
);

/*
01 - Daily Pickup (default)
03 - Customer Counter
06 - One Time Pickup
07 - On Call Air
19 - Letter Center
20 - Air Service Center
*/
$pwm_awsp_config['ups']['pickup_type'] = '01'; 

/*
00 - Rates Associated with Shipper Number
01 - Daily Rates
04 - Retail Rates
53 - Standard List Rates
*/
$pwm_awsp_config['ups']['rate_type'] = '00'; 

//----------------------------------------------------------------------------------------------------------------------


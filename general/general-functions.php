<?php
/**
 * The core function file.
 *
 * This is used to define general wp specific functions.
 * public-facing site hooks.
 *
 *
 * @since      1.0.0
 * @package    pwm-shipments
 * @subpackage pwm-shipments/general
 * @author     PWM
 */
require_once(ABSPATH . 'wp-load.php'); 

/**
 * Define the redirect function to redirect from back-end pages.
 */
function pwmWPRedirct($location)
{
	if (!headers_sent()) {
        header("Location: " . $location);
    }  else {
        wp_redirect($location);
        exit();
    }
}
/**
 * Force trailing slash no matter what. does not work with files/query parameters
 * @param type $input
 * @return type
 */
function pwmWPForceTrailingSlash($input)
{
    if (substr($input, -1) !== '/') {
        $input .= '/';
    }
    return $input;
}

/**
 * 
 * @param array|String $attributes - 
 *      array of values that will then be encoded to html attributes
 *      raw html attributes string
 * @param array $options
 *      Array where keys are  option values and values are option labels
 *      Optgroups can exist - key is label value is options
 *      ex: [
 *              'caleb', 'black' => 'John Snow', 10 => 'That guy', 
 *              'People To Ignore Phone Calls from' => [
 *                  'Stalin', 'Hitler', 'North Korea'
 *              ]
 *          ]
 * @param array $selected The default selected value if exists
 */
function pwm_shipments_general_frontend_select($attributes, array $options, $selected = null)
{
    $attrs = is_array($attributes) ? pwmGetHtmlAttrsFromArr($attributes) : $attributes;
    
    printf("<select %s >", $attrs);
    $printOption = function($k, $selected, $v) {
        printf("<option value=\"%s\" %s>%s</option>", $k, $selected, $v);
    };
    $getSelected = function($value) use ($selected) {
        return ($value == $selected) ? "selected=\"selected\"" : "";
    };
    foreach($options as $key => $value) {
        // if element is array then add optgroup
        if( is_array($value) ) {
            printf("<optgroup label=\"%s\">", $key);
            foreach( $value as $k => $v ) {
                $printOption($key, $getSelected($k), $v);
            }
            printf("</optgroup>");
        } else {
            //normal option
            $printOption($key, $getSelected($key), $value);
        }
    }
    printf("</select>");
}
/**
 * Check date formate on packling list filter
 */
function pwm_shipments_validateDateString($date, $format = "Y-m-d")
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

/**
 * Create packing list report and define row styles
 */
function pwm_shipments_general_pdf_get_row_style($row)
{
    $style = "font-size:large;padding:2px;";
    $style .= ( $row % 2 === 1 ) ? "background-color: rgb(204, 204, 204);" : "background-color: rgb(255, 255, 255);";
    return $style;
}

/**
 * Create packing list report in pdf format and write content to pdf
 */
function pwm_shipments_general_write_contents_to_pdf($fileName, $content, $destination = "F")
{
    require_once PWM_SHIPMENTS_PLUGIN_DIR . 'assets/tcpdf/tcpdf.php';
    
    $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Pro Web Marketing');
    $pdf->SetTitle($fileName);
    $pdf->SetSubject('Shipping Labels');
    
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    
    // set font
    $pdf->SetFont('times', '', 14);

    // add a page
    $pdf->AddPage();
    $pdf->writeHTML( nl2br($content), true, false, true, false, '');
    $pdf->Output($fileName, $destination);
}

/**
 * Get akademos shipment price per unit
 */
function pwm_shipments_akademos_get_price_per_unit($bookCode)
{
    if( ! isset($GLOBALS['pwm-akademos-inventory-arr']) ) {
        $GLOBALS['pwm-akademos-inventory-arr'] = get_option('pwm-shipping-akademos-inventory-arr', array());
    }
    
    $price = 0.00;
    foreach( $GLOBALS['pwm-akademos-inventory-arr'] as $row ) {
        if( $row[1] === $bookCode ) {
            $price = $row[2];
            break;
        }
    }
    return $price;
}

/**
 * create zips and download it
 * IMPORTANT must be called before headers sent
 * @param array $files
 * @param bool $deleteAfter
 */
function pwm_shipments_general_download_all_files($files, $deleteAfter = false)
{
    # create new zip opbject
    $zip = new ZipArchive();
    
    # create a temp file & open it
    $tmpFile = uniqid( PWM_SHIPMENTS_PLUGIN_TMP_DIR . 'labels-') . '.zip';
    $zip->open($tmpFile, ZipArchive::CREATE);

    $zip = pwm_shipments_general_download_add_files_to_zip($zip, $files);
    
    # close zip
    $zip->close();
    
    # send the file to the browser as a download
    header('Content-disposition: attachment; filename=labels.zip');
    header('Content-type: application/zip');
    readfile($tmpFile);
    
    if( $deleteAfter ) {    
        foreach( $files as $file ) {
    unlink($file);
        }
        unlink($tmpFile);
    }
    
    die;
}

/**
 * [pwm_shipments_general_download_add_files_to_zip description]
 * @param  [type] $zip     [description]
 * @param  [type] $files   [description]
 * @param  string $baseDir [description]
 * @return [type]          [description]
 */
function pwm_shipments_general_download_add_files_to_zip($zip, $files, $baseDir = "")
{
    # loop through each file
    foreach($files as $key => $file){
        if( is_array($file) ) {
            $zip->addEmptyDir($key);
            $newBaseDir = $baseDir . pwmWPForceTrailingSlash($key);
            $zip = pwm_shipments_general_download_add_files_to_zip($zip, $file, $newBaseDir);
        } else {
            # download file
            $downloadFile = file_get_contents($file);
            #add it to the zip
            $zip->addFromString( $baseDir . basename($file), $downloadFile);
        }
    }
    return $zip;
}

/**
 * create pdf for packing list
 * @return object
 */
function pwm_shipments_tcpdf_packing_list_class()
{
    if (class_exists('PWM_Shipments_TCPDF_Packing_List'))
        return;

    require_once PWM_SHIPMENTS_PLUGIN_ROOT_DIR . 'library/tcpdf/tcpdf.php';

    class PWM_Shipments_TCPDF_Packing_List extends TCPDF {
        
        //Page header
        public function Header() {
            $this->SetY(5);
            // Logo
            $header = "\n" . date('m/d/Y');
            $this->Cell(0, 0, $header, 0, 0, 'C');
        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            $footer = sprintf(
                    "Page %s of %s",
                    $this->getAliasNumPage(), $this->getAliasNbPages());
            $this->Cell(0, 0, $footer, 0, 0, 'C');
        }

    }
}

/**
 * Send emails using wp_mail
 * @param  String $from
 * @param  String $to
 * @param  String $subject
 * @param  String $message
 * @param  string $headers
 * @return Boolean
 */
function pwmShipmentsHtmlEmail($from, $to, $subject, $message, $headers = "")
{
    if( is_null($from) ) {
        $from = get_option( 'admin_email' ); 
    }
    $headers = "From: " . strip_tags($from) . "\r\n";
    $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
    
    wp_mail($to, $subject, $message, $headers);
}
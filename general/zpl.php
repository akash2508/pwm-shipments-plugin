<?php

use Zebra\Client;
use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;
use Zebra\Zpl\GdDecoder;

function pwmConvertGifToZPL($gifContents) {
    $zplOutput = false;
    
    try {
        if( strlen($gifContents) > 0 ) {
            $decoder = GdDecoder::fromString($gifContents);
            $image = new Image($decoder);
            $zpl = new Builder();
            $zpl->fo(0, 0)->gf($image)->fs();

            $zplOutput = $zpl->__toString();
        }
    } catch( Exception $e ) {
        $zplOutput = false;
    }
    return $zplOutput;
}
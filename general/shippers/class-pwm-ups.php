<?php
/**
 * The UPS Shipping functionality of the plugin.
 *
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/general/shippers
 * @author     PWM
 */

use \Awsp\Ship as Ship;

require_once(PWM_SHIPMENTS_PLUGIN_ROOT_DIR. '/general/config/awsp-config.php');

require_once(PWM_SHIPMENTS_PLUGIN_ROOT_DIR. '/vendor/awsp/ship/includes/autoloader.php');

class PWM_UPS
{
    protected $pwm_awsp_config;

    protected $inputFile, $inputLine, $shippingMethod, $trackingNumber, $label, $serviceCode;

    protected $weight;
    
    protected $fullname;

    protected $address1;

    protected $address2;

    protected $city;

    protected $state;

    protected $zip;

	/**
	 * Run a constructor.
	 *
	 * @return Void
	 */
    public function __construct()
    {
    	global $pwm_awsp_config;

        $this->pwm_awsp_config = $pwm_awsp_config;

    }

    /**
     * @param string $inputFile
     *
     * @return $this
     */
    public function setInputFile($inputFile)
    {
        $this->inputFile = $inputFile;

        return $this;
    }

    /**
     * @param string $inputLine
     *
     * @return $this
     */
    public function setInputLine($inputLine)
    {
        $this->inputLine = $inputLine;

        return $this;
    }

    /**
     * @param string $shippingMethod
     *
     * @return $this
     */
    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;

        $this->getUpsServiceCode($shippingMethod);

        return $this;
    }

    /**
     * @param string $weight
     *
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @param string $fullname
     *
     * @return $this
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @param string $address1
     *
     * @return $this
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @param string $address2
     *
     * @return $this
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @param string $zip
     *
     * @return $this
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @param string $shippingMethod
     *
     * @return $this
     */
	public function getUpsServiceCode($shippingMethod)
	{
		if ($shippingMethod === 'G') {
	        $this->serviceCode = '03';
	    } else if ($shippingMethod === 'NS') {
	        $this->serviceCode = '13';
	    } else if ($shippingMethod === 'N') {
	        $this->serviceCode = '01';
	    } else if ($shippingMethod === '2') {
	        $this->serviceCode = '02';
	    } else {
	        $this->serviceCode = null;
	    }

	    return $this;
	}

    public function createLabel()
    {
	    if ($this->serviceCode === null) {
	        return FALSE;
	    }

	    if (strlen($this->fullname) > 35)
        	$this->fullname = substr($this->fullname, 0, 35);

        $shipmentData = array();

        // shipping from location information (if different than shipper's information)
		$shipmentData['ship_from_different_address'] = false;
		// if $shipmentData['ship_from_different_address'] is true, the following information must be completed
		$shipmentData['shipping_from_name'] = null;
		$shipmentData['shipping_from_phone'] = null;
		$shipmentData['shipping_from_email'] = null;
		$shipmentData['shipping_from_address1'] = null;
		$shipmentData['shipping_from_address2'] = null;
		$shipmentData['shipping_from_address3'] = null;
		$shipmentData['shipping_from_city'] = null;
		$shipmentData['shipping_from_state'] = null;
		$shipmentData['shipping_from_postal_code'] = null;
		$shipmentData['shipping_from_country_code'] = null;

		// receiver information
		$shipmentData['receiver_name'] = $this->fullname;
		$shipmentData['receiver_attention_name'] = $this->fullname;
		$shipmentData['receiver_phone'] = '555-123-4567';
		$shipmentData['receiver_address1'] = $this->address1;
		$shipmentData['receiver_address2'] = $this->address2;
		$shipmentData['receiver_city'] = $this->city;
		$shipmentData['receiver_state'] = $this->state;
		$shipmentData['receiver_postal_code'] = $this->zip;
		$shipmentData['receiver_country_code'] = 'US';
		$shipmentData['receiver_is_residential'] = true;

		try {
	        $Shipment = new Ship\Shipment($shipmentData); // create a Shipment object
	    } catch (\Exception $e) {
	        exit('<br /><br />Error: ' . $e->getMessage() . '<br /><br />');
	    }

	    // create a Package object and add it to the Shipment (a shipment can have multiple packages)
		// this package is 24 pounds, has dimensions of 10 x 6 x 12 inches, has an insured value of $274.95 and is being sent 
		//      signature required
		try {

			$dimensions = array(0, 0, 0); // inches array
	        $insuredAmount = 0;
	        $options = array(
	            'signature_required' => false,
	            'insured_amount' => $insuredAmount
	        );
		    $Package1 = new Ship\Package(
		            $this->weight, // pounds
	                $dimensions, //inches array
	                $options
		        );
		    $Shipment->addPackage($Package1);
		}
		// catch any exceptions 
		catch(\Exception $e) {
		    exit('<br /><br />Error: ' . $e->getMessage() . '<br /><br />');    
		}

	    try {

	        // interface with the desired shipper plugin object
	        $ShipperObj = new Ship\Ups($Shipment, $this->pwm_awsp_config);

	        // build parameters array to send to the createLabel method
	        $params = array(
	            'service_code' => $this->serviceCode
	        );

	        $response = $ShipperObj->createLabel($params);

	        $this->trackingNumber = null;

	        $this->label = null;

	        if (count($response->labels) === 1) {
	            $this->trackingNumber = $response->labels[0]['tracking_number'];
	            $this->label = $response->labels[0]['label_image'];
	        } else {
	            var_dump($response);
	        }

	        $shipmentMethod = isset($ShipperObj->services[$this->serviceCode]) ? $ShipperObj->services[$this->serviceCode] : $this->serviceCode;

            if ($shipmentMethod == '03') {
                $shipmentMethod = 'Ground';
            }
            
	    } catch (Exception $e) {
	        return false;
	    }

	    //rotate image to be portrait
	    $imgRaw = imagecreatefromstring(base64_decode($this->label));

	    $imgRotated = imagerotate($imgRaw, 270, 0);

	    ob_start();

	    imagegif($imgRotated, null);

	    $gifRotated = ob_get_clean();

	    $akademosOrderData = array(
	        "shipper" => "UPS",
	        "shipment_method" => $shipmentMethod,
	        "tracking" => $this->trackingNumber,
	        "label_type" => "zpl",
	        // UPS doesn't create ZPL files for Cash On Delivery Orders... so have to convert manually
	        "label_base64" => base64_encode(pwmConvertGifToZPL($gifRotated)),
	        "response" => serialize($response),
	        "input_line" => $this->inputLine,
	        "input_file" => $this->inputFile,
	    );

	    return array($akademosOrderData, $this->trackingNumber);

    }
}
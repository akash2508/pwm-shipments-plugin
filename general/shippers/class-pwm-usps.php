<?php
/**
 * The USPS Shipping functionality of the plugin.
 *
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/general/shippers
 * @author     PWM
 */

use ag2508\stamps\address\Address as Address;

use ag2508\stamps\api\Indicium as Indicium;


class PWM_USPS
{
    /**
     * The stamps connection.
     * @var IndiciumInterface
     */
    protected $stamps;

    /**
     * The recipient's address.
     * @var AddressInterface
     */
    protected $toAddress;

    /**
     * The sender's adddress.
     * @var AddressInterface
     */
    protected $fromAddress;

    protected $inputFile, $inputLine, $shipDate, $trackingNumber, $label, $insuredValue;

    /**
     * The mail service type.
     * @var string
     */
    protected $serviceType;

    protected $weightLb;

    protected $length;

    protected $width;

    protected $height;
    
    protected $fullname;

    protected $address1;

    protected $address2;

    protected $city;

    protected $state;

    protected $zip;

    protected $zipAddon;

	/**
	 * Run a constructor.
	 *
	 * @return Void
	 */
    public function __construct()
    {

        $this->shipDate = date("Y-m-d");
	    $this->length = null;
	    $this->width = null;
	    $this->height = null;
	    $this->insuredValue = null;
	    $this->trackingNumber = false;
	    $this->label = '';

	    $this->stamps = (new Indicium())
            ->setApiUrl('https://swsim.stamps.com/swsim/swsimv90.asmx')
            ->setApiIntegrationId(PWM_SHIPMENTS_STAMPS_COM_API_INTEGRATION_ID)
            ->setApiUserId(PWM_SHIPMENTS_STAMPS_COM_API_USER)
            ->setApiPassword(PWM_SHIPMENTS_STAMPS_COM_API_PASS)
            ->setImageType(Indicium::IMAGE_TYPE_AZPL)
            ->setPackageType(Indicium::RATE_PACKAGE_TYPE_PACKAGE)
            ->setIsSampleOnly(false)
            ->setShipDate($this->shipDate);

    }

    /**
     * @param string $inputFile
     *
     * @return $this
     */
    public function setInputFile($inputFile)
    {
        $this->inputFile = $inputFile;

        return $this;
    }

    /**
     * @param string $inputLine
     *
     * @return $this
     */
    public function setInputLine($inputLine)
    {
        $this->inputLine = $inputLine;

        return $this;
    }

    /**
     * @param string $serviceType
     *
     * @return $this
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;

	    $this->stamps->setServiceType($this->serviceType);

        return $this;
    }

    /**
     * @param string $weight
     *
     * @return $this
     */
    public function setWeightLb($weight)
    {
        $this->weightLb = $weight;

	    $this->stamps->setWeightLb($this->weightLb);

        return $this;
    }

    /**
     * @param string $fullname
     *
     * @return $this
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @param string $address1
     *
     * @return $this
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @param string $address2
     *
     * @return $this
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @param string $zip
     *
     * @return $this
     */
    public function setZipcode($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @param string $zipAddon
     *
     * @return $this
     */
    public function setZIPcodeAddOn($zipAddon)
    {
        $this->zipAddon = $zipAddon;

        return $this;
    }

    public function setToAddress()
    {
    	$this->toAddress = (new Address())
	        ->setFullname($this->fullname)
	        ->setAddress1($this->address1)
	        ->setAddress2($this->address2)
	        ->setCity($this->city)
	        ->setState($this->state)
	        ->setZipcode($this->zip)
	        ->setZIPcodeAddOn($this->zipAddon);

	    $this->stamps->setTo($this->toAddress);

	    return $this;
    }

    public function setFromAddress()
    {
    	$this->fromAddress = (new Address())
	        ->setFullname(PWM_SHIPMENTS_AKADEMOS_SHIPPER_NAME)
	        ->setAddress1(PWM_SHIPMENTS_AKADEMOS_SHIPPER_ADDRESS_1)
	        ->setAddress2(PWM_SHIPMENTS_AKADEMOS_SHIPPER_ADDRESS_2)
	        ->setCity(PWM_SHIPMENTS_AKADEMOS_SHIPPER_CITY)
	        ->setState(PWM_SHIPMENTS_AKADEMOS_SHIPPER_STATE)
	        ->setZipcode(PWM_SHIPMENTS_AKADEMOS_SHIPPER_POSTAL_CODE);

	    $this->stamps->setFrom($this->fromAddress);

	    return $this;

    }

    public function createLabel()
    {
	    $response = $this->stamps->create();

	    if ( is_string($response) && strpos($response, "Insufficient postage") === 0 ) {
	    	$purchasePostageCount = get_option('pwm-akademos-daily-cron-usps-purchase-count', 0);
	    	if( $purchasePostageCount < 8 ) {
	    		$boughtPostage = $this->stamps->purchasePostage();
	    		update_option('pwm-akademos-daily-cron-usps-purchase-count', $purchasePostageCount);
	    	} else {
                $boughtPostage = false;
            }

            if ($boughtPostage) {
                //if bought postage retry creating label one time
                $response = $this->stamps->create();
            } else {
                return false;
            }
	    } else if (is_object($response)) {
            if (is_string($response->TrackingNumber) && strlen($response->TrackingNumber) > 0) {
                $this->trackingNumber = $response->TrackingNumber;
            }
            if (isset($response->ImageData) && !empty($response->ImageData->base64Binary)) {
                $this->label = $response->ImageData->base64Binary;
            }
        } else {
            return false;
        }

        return $response;
    }

    public function orderData()
    {
    	$response = $this->createLabel();

    	$akademosOrderData = array(
	        "shipper" => "Stamps.com",
	        "shipment_method" => $this->serviceType,
	        "tracking" => $this->trackingNumber,
	        "label_type" => "zpl",
	        "label_base64" => base64_encode($this->label),
	        "response" => serialize($response),
	        "input_line" => $this->inputLine,
	        'input_file' => $this->inputFile,
	    );

	    return array($akademosOrderData, $this->trackingNumber);
    }

}
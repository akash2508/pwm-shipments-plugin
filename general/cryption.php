<?php
/**
 * The OpenSSL cryption functionality of the plugin.
 *
 *
 * @package    pwm-shipments
 * @subpackage pwm-shipments/general
 * @author     PWM
 */

class PWM_Cryption
{

    protected $ciphering;

    protected $options;

	/**
	 * Run a constructor.
	 *
	 * @return Void
	 */
    public function __construct()
    {
    	$this->ciphering = "AES-128-CTR"; 

    	$this->options = 0;
    }

	/**
	 * Realitively secure functiuon to encrypt and decrypt strings
	 * @param String $password
	 * @param mixed $data - the data to encrypt
	 * @return String - the encrypted string
	 */
	public function pwm_general_encrypt($password, $data) {

	    $dataStr = serialize($data);
	    $salt = substr(md5(mt_rand(), true), 8);
	    $encryption_key = md5($password . $salt, true);
	    $encryption_iv  = md5($encryption_key . $password . $salt, true);

		$encryption = openssl_encrypt($dataStr, $this->ciphering, $encryption_key, $this->options, $encryption_iv);

		$encrypted = base64_encode('Salted__' . $salt . $encryption);

	    return $encrypted;
	}
	/**
	 * To decrypt 
	 * @param String $password
	 * @param String $dataStr
	 * @return mixed 
	 */
	public function pwm_general_decrypt($password, $dataStr) {

	    $data = base64_decode($dataStr);
	    $salt = substr($data, 8, 8);
	    $encryption = substr($data, 16);

	    $decryption_key = md5($password . $salt, true);
	    $decryption_iv = md5($decryption_key . $password . $salt, true);

		$decryption = openssl_decrypt($encryption, $this->ciphering, $decryption_key, $this->options, $decryption_iv);

	    return unserialize( $decryption );
	}

}
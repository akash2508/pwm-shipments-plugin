<?php
/**
 * @wordpress-plugin
 * Plugin Name:       PWM Shipments
 * Plugin URI:        https://coursepacksetc.com
 * Description:       This plugin has been developed for process Akademos PO files to read/write orders and generate tracking numbers.
 * Version:           3.0.0
 * Author:            PWM
 * Author URI:        https://coursepacksetc.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pwm-shipments
 *
 * Plugin Variable: pwm
 *
 */

if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly

if (! defined( 'WPINC' )) {
    die;
}

/**
 * Defing plugin's root directory and url
 */
define('PWM_SHIPMENTS_PLUGIN_ROOT_DIR', WP_PLUGIN_DIR.'/pwm-shipments/' );
define('PWM_SHIPMENTS_PLUGIN_ROOT_URL', WP_PLUGIN_URL.'/pwm-shipments/' );


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woocommerce-advanced-shipping-activator.php
 */

function activate_pwm_shipments()
{
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-pwm-shipments-activator.php';

    PWM_Shipments_Activator::activate();
    flush_rewrite_rules();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woocommerce-advanced-shipping-deactivator.php
 */
function deactivate_pwm_shipments()
{
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-pwm-shipments-deactivator.php';
    PWM_Shipments_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pwm_shipments' );
register_deactivation_hook( __FILE__, 'deactivate_pwm_shipments' );

/**
 * Load all the libraries from vendor.
 */
require_once(PWM_SHIPMENTS_PLUGIN_ROOT_DIR. '/vendor/autoload.php');

/**
 * The core plugin class that is used to define core pages,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pwm-main-shipments.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wpm_shipments()
{
    $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );

    if ( in_array( 'woocommerce/woocommerce.php', $active_plugins) ) {
        $plugin = new PWM_Main_Shipments();
        $plugin->run();
    } else {
        add_action( 'admin_notices', 'pwm_shipments_admin_notice' );
        function pwm_shipments_admin_notice() { ?>
            <div class="notice error pwm-woo-notice is-dismissible" >
                <p><?php _e( 'Woocommerce plugin is required to include PWM Shipments plugin.', 'pwm-shipments' ); ?></p>
            </div>
            <?php
        }
    }
}

run_wpm_shipments();